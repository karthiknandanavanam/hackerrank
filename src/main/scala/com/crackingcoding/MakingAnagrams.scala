package com.crackingcoding

object MakingAnagrams {

  def update(arr: Array[(Int, Int)], c: Char)(f: Boolean = true) = {
    val ind = c - 97
    val tuple = arr(ind)
    f match {
      case true => arr(ind) = (tuple._1 + 1, tuple._2)
      case false => arr(ind) = (tuple._1, tuple._2 + 1)
    }
  }

  def main(args: Array[String]) = {
    val arr = new Array[(Int, Int)](26)
    for (i <- 0 until 26) {
      arr(i) = (0, 0)
    }

    val scan = new java.util.Scanner(System.in)
    val a = scan.next()
    val b = scan.next()

    a.foreach(c => update(arr, c)(f = true))
    b.foreach(c => update(arr, c)(f = false))
    val total = a.length + b.length
    val common = arr.map(a => Math.min(a._1, a._2)).sum * 2
    println(total - common)
  }
}
