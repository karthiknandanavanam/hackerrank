package com.crackingcoding

object BubbleSort {

  def bubbleSort(arr: Array[Int]) = {
    var swaps = 0
    for (i <- 0 until arr.length) {
      for (j <- 0 until arr.length - i - 1) {
        if (arr(j) > arr(j + 1)) {
          val temp = arr(j)
          arr(j) = arr(j + 1)
          arr(j + 1) = temp
          swaps = swaps + 1
        }
      }
    }
    swaps
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val swaps = bubbleSort(arr)
    println("Array is sorted in "+ swaps +" swaps.")
    println("First Element: " + arr(0))
    println("Last Element: " + arr(size - 1))
  }
}
