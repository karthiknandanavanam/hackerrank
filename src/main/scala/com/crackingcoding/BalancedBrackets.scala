package com.crackingcoding

object BalancedBrackets {

  def checkValidity(b: String): Boolean = {
    val stack = scala.collection.mutable.Stack[Char]()
    for (a <- b) {
      a match {
        case '{' => stack.push(a)
        case '(' => stack.push(a)
        case '[' => stack.push(a)
        case '}' => {
          if (stack.isEmpty) return false
          val c = stack.head
          c match {
            case '{' => stack.pop()
            case _ => return false
          }
        }
        case ')' => {
          if (stack.isEmpty) return false
          val c = stack.head
          c match {
            case '(' => stack.pop()
            case _ => return false
          }
        }
        case ']' => {
          if (stack.isEmpty) return false
          val c = stack.head
          c match {
            case '[' => stack.pop()
            case _ => return false
          }
        }
      }
    }
    stack match {
      case a if a.isEmpty => true
      case a => false
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val arr = new Array[String](size)
    for (i <- 0 until size) {
      arr(i) = scan.next()
    }

    for (i <- 0 until size) {
      val brackets = arr(i)
      if (checkValidity(brackets)) println("YES") else println("NO")
    }
  }
}
