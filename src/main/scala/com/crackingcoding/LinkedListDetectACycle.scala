package com.crackingcoding

object LinkedListDetectACycle {

  trait LinkedList {
    def isEmpty(): Boolean
    def ne(): LinkedList
    def add(a: Int): LinkedList = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => node.next = this; node
      }
    }

    def isCycle() = {
      var tortoise = this
      var hare = this.ne()
      while (tortoise != hare && !tortoise.isEmpty() && !hare.isEmpty()) {
        tortoise = tortoise.ne()
        hare match {
          case a if a.ne().isEmpty() => hare = a.ne
          case a => hare = a.ne.ne
        }
      }

      (hare, tortoise) match {
        case (a, b) if a == b => true
        case _ => false
      }
    }

    def cycle(pos: Int) = {
      var cyNode = this
      for (i <- 0 until pos) {
        cyNode = cyNode.ne
      }
      var node = this
      while (!node.ne().isEmpty()) {
        node = node.ne
      }
      node match {
        case a: NonEmpty => a.next = cyNode
      }
      this
    }
  }

  class NonEmpty(n: Int) extends LinkedList {
    var next: LinkedList = new Empty
    override def ne() = next
    override def isEmpty(): Boolean = false
  }

  class Empty extends LinkedList {
    override def ne(): LinkedList = throw new UnsupportedOperationException
    override def isEmpty(): Boolean = true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    var ll: LinkedList = new Empty
    for (i <- 0 until size) {
      val num = scan.nextInt()
      ll = ll.add(num)
    }

    val cyPos = scan.nextInt()
    ll.cycle(cyPos)

    println(ll.isCycle())
  }
}
