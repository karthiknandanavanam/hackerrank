package com.crackingcoding

object QueuesTwoStacks {

  class Queue {
    val en = scala.collection.mutable.Stack[Int]()
    val de = scala.collection.mutable.Stack[Int]()

    def enqueue(a: Int) = {
      en.push(a)
    }

    def dequeue(): Int = {
      if (de.nonEmpty) {
        de.pop()
      }
      else {
        while (en.nonEmpty) {
          val num = en.pop()
          de.push(num)
        }
        de.pop()
      }
    }

    def peek(): Int = {
      if (de.nonEmpty) {
        de.head
      }
      else {
        while (en.nonEmpty) {
          val num = en.pop()
          de.push(num)
        }
        de.head
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt

    val arr = new Array[String](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextLine()
    }

    val q = new Queue()
    for (i <- 0 until size) {
      val cols = arr(i).split(" ")
      val first = cols(0).toInt
      first match {
        case 1 => {
          val second = cols(1).toInt
          q.enqueue(second)
        }
        case 2 => q.dequeue()
        case 3 => println(q.peek())
      }
    }
  }
}
