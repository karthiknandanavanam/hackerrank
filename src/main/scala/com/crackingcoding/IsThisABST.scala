package com.crackingcoding

import scala.collection.mutable

object IsThisABST {

  trait Tree {
    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.data
      }
    }

    def left: Tree = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le
      }
    }

    def right: Tree = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri
      }
    }

    def addRight(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri = node
      }
    }

    def addLeft(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le = node
      }
    }

    def isEmpty = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def display: String = {
      var res = ""
      def inOrder(node: Tree): Unit = {
        node match {
          case a: Empty => res = res
          case _ => {
            inOrder(node.left)
            res = res + node.data + " "
            inOrder(node.right)
          }
        }
      }
      inOrder(this)
      res
    }

    def isBST: Boolean = {
      val queue = new mutable.Queue[Int]()
      def inOrder(node: Tree): Unit = {
        node match {
          case a: NonEmpty => {
            inOrder(node.left)
            queue.enqueue(node.data)
            inOrder(node.right)
          }
          case _ =>
        }
      }
      inOrder(this)
      if (queue.isEmpty || queue.size == 1) {
        true
      }
      else {
        val n = queue.size
        for (i <- 1 until n) {
          val s = queue.dequeue()
          if (queue.isEmpty) {
            return true
          }
          else if (queue.head <= s) {
            return false
          }
        }
        return true
      }
    }

    def insert(a: Int): Tree = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          var iter = this
          while (!iter.isEmpty) {
            if (node.data < iter.data && iter.left.isEmpty) {
              iter.addLeft(node)
              iter = new Empty
            }
            else if (node.data > iter.data && iter.right.isEmpty) {
              iter.addRight(node)
              iter = new Empty
            }
            else if (node.data < iter.data) {
              iter = iter.left
            }
            else if (node.data > iter.data) {
              iter = iter.right
            }
            else if (node.data == iter.data) {
              return this
            }
          }
          return this
        }
      }
    }
  }

  class NonEmpty(a: Int) extends Tree {
    var le: Tree = new Empty
    var ri: Tree = new Empty
    override def data = a
  }

  class Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var bst: Tree = new Empty
    for (i <- 0 until size) {
      bst = bst.insert(arr(i))
    }

    println(bst.display)
    println(bst.isBST)

    val a4: NonEmpty = new NonEmpty(4)
    val a2: NonEmpty = new NonEmpty(2)
    val a6: NonEmpty = new NonEmpty(6)
    val a1: NonEmpty = new NonEmpty(1)
    val a3: NonEmpty = new NonEmpty(3)
    val a5: NonEmpty = new NonEmpty(5)
    val a7: NonEmpty = new NonEmpty(7)

    a3.le = a2
    a3.ri = a6
    a2.le = a1
    a2.ri = a4
    a6.le = a5
    a6.ri = a7

    println(a3.display)
    println(a3.isBST)
  }

}
