package com.crackingcoding

object ArraysLeftRotation {

  def rotate(arr: Array[Int]) = {
    val m = arr(0)
    for (i <- 1 until arr.length) {
      arr(i - 1) = arr(i)
    }
    arr(arr.length - 1) = m
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val rotN = scan.nextInt()

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    for (i <- 0 until rotN) {
      rotate(arr)
    }
    println(arr.mkString(" "))
  }
}
