package com.crackingcoding

object RansomNote {

  def isValid(b: Array[String], map: scala.collection.mutable.HashMap[String, Int]): Boolean = {
    for (i <- 0 until b.length) {
      val s = b(i)
      if (map.contains(s) && map(s) >= 1) map.put(s, map(s) - 1) else return false
    }
    return true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val aSize = scan.nextInt()
    val bSize = scan.nextInt()
    val map = new scala.collection.mutable.HashMap[String, Int]()
    for (i <- 0 until aSize) {
      val s = scan.next()
      if (map.contains(s)) map.put(s, map(s) + 1) else map.put(s, 1)
    }

    val arr = new Array[String](bSize)
    for (i <- 0 until bSize) {
      arr(i) = scan.next()
    }

    val res = isValid(arr, map)

    res match {
      case true => println("Yes")
      case false => println("No")
    }
  }
}
