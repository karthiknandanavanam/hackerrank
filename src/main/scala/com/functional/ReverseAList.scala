package com.functional

object ReverseAList {

  def f(arr: List[Int]): List[Int] = {
    arr match {
      case a if a.isEmpty => List()
      case _ => f(arr.tail) ++ List(arr.head)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val rev = f(arr.toList)
    println(rev.mkString(" "))
  }
}
