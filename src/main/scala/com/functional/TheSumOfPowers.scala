package com.functional

object TheSumOfPowers {

  def combs(nums: Array[Int], x: Int, sum: Int): Int = {
    (nums, sum) match {
      case (_, b) if b == x => 1
      case (a, _) if a.isEmpty => 0
      case (a, b) if a.nonEmpty && b > x => 0
      case (a, _) if a.nonEmpty => combs(a.tail, x, sum + a.head) + combs(a.tail, x, sum)
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2).map(r => r.toInt).toArray
    val x = inputs(0)
    val n = inputs(1)

    val pows = (1 to x).map(r => Math.pow(r, n).toInt).takeWhile(r => r <= x).toArray
    val count = combs(pows, x, 0)
    println(count)
  }
}
