package com.functional

object SolveMeFirstFP {
  def main(args: Array[String]) = {
    println(io.Source.stdin.getLines().take(2).map(rec => rec.toInt).sum)
  }
}
