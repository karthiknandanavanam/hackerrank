package com.functional

object ListLength {

  def f(arr: List[Int]): Int = {
    arr match {
      case a if a.isEmpty => 0
      case _ => 1 + f(arr.tail)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val length = f(arr.toList)
    println(length)
  }
}
