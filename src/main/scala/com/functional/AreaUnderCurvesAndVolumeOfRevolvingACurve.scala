package com.functional

object AreaUnderCurvesAndVolumeOfRevolvingACurve {

  def f(coef: List[Int], pow: List[Int], x: Double) = {
    coef.zip(pow).map(rec => rec._1 * Math.pow(x, rec._2)).sum
  }

  def ar(coefficients:List[Int],powers:List[Int],x:Double): Double = {
    f(coefficients, powers, x)
  }

  def area(coefficients:List[Int],powers:List[Int],x:Double): Double = {
    Math.PI * Math.pow(f(coefficients, powers, x), 2)
  }

  def summation(func:(List[Int],List[Int],Double)=>Double, upperLimit:Int, lowerLimit:Int, coefficients:List[Int], powers:List[Int]):Double = {
    val step = 0.001
    (lowerLimit.toDouble to upperLimit.toDouble by step).map(rec => func(coefficients, powers, rec) * step).sum
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val coef = scan.nextLine().split(" ").map(rec => rec.toInt).toList
    val pow = scan.nextLine().split(" ").map(rec => rec.toInt).toList
    val lAndR = scan.nextLine().split(" ").toList
    val l = lAndR(0).toInt
    val r = lAndR(1).toInt

    val are = summation(ar, r, l, coef, pow)
    val vol = summation(area, r, l, coef, pow)
    println("%.1f".format(are))
    println("%.1f".format(vol))
  }
}
