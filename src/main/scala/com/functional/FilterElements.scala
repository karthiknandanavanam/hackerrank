package com.functional

object FilterElements {

  def filter(in: List[Int], r: Int, m: scala.collection.mutable.LinkedHashMap[Int, Int]): Array[Int] = {
    in match {
      case a if a.isEmpty => val res = m.filter(b => b._2 >= r).keys.toArray; if (res.isEmpty) Array(-1) else res
      case a if a.nonEmpty => m.put(a.head, m.getOrElse(a.head, 0) + 1); filter(in.tail, r, m)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val results = (0 until size) map {_ =>
      val (s, r) = (scan.nextInt(), scan.nextInt())
      val list = (0 until s).map(_ => scan.nextInt()).toList
      val res = filter(list, r, scala.collection.mutable.LinkedHashMap())
      res
    }

    results foreach {a => println(a.mkString(" "))}
  }
}
