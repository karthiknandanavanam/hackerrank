package com.functional

import scala.collection.mutable

object SequenceFullOfColors {

  def validity(input: List[Char], map: mutable.HashMap[Char, Int]): Boolean = {
    (input, map) match {
      case (_, b) if Math.abs(b.getOrElse('R', 0) - b.getOrElse('G', 0)) > 1 || Math.abs(b.getOrElse('Y', 0) - b.getOrElse('B', 0)) > 1 => false
      case (a, b) if a.isEmpty && b.getOrElse('R', 0) == b.getOrElse('G', 0) && b.getOrElse('Y', 0) == b.getOrElse('B', 0) => true
      case (a, _) if a.isEmpty => false
      case (a, _) if a.nonEmpty => map.put(a.head, map.getOrElse(a.head, 0) + 1); validity(a.tail, map)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt
    val inputs = (0 until size).map(_ => scan.nextLine()).toArray
    val outputs = inputs.map(r => validity(r.toList, mutable.HashMap())).map(r => if (r) "True" else "False")
    println(outputs.mkString("\n"))
  }
}
