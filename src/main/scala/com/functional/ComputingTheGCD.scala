package com.functional

object ComputingTheGCD {

  def gcd(a: Int, b: Int): Int = {
    (a, b) match {
      case (n, 0) => n
      case (n, m) if n < m => val div = m / n; val rem = m % n; gcd(n, rem)
      case (n, m) if n > m => val div = n / m; val rem = n % m; gcd(m, rem)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val a = scan.nextInt()
    val b = scan.nextInt()
    val res = gcd(a, b)
    println(res)
  }
}
