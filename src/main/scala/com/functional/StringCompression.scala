package com.functional

object StringCompression {

  def compress(in: String, out: StringBuilder): StringBuilder = {
    in match {
      case a if a.isEmpty => out
      case a => {
        val char = a.head
        val size = a.takeWhile(r => r == char).length
        if (size == 1) {
          out.append(char)
          compress(a.tail, out)
        }
        else {
          out.append(char)
          out.append(size)
          compress(a.dropWhile(r => r == char), out)
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val input = scala.io.Source.stdin.getLines().next
    val output = compress(input, new StringBuilder()).toString()
    println(output)
  }
}
