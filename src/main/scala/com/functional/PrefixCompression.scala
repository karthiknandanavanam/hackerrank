package com.functional

object PrefixCompression {

  def compress(a: String, b: String, common: StringBuilder): (String, String, String) = {
    (a, b) match {
      case (l, r) if l.isEmpty && r.isEmpty => (common.toString(), l, r)
      case (l, r) if l.isEmpty && r.nonEmpty => (common.toString(), l, r)
      case (l, r) if l.nonEmpty && r.isEmpty => (common.toString(), l, r)
      case (l, r) if l.nonEmpty && r.nonEmpty && l.head != r.head => (common.toString, l, r)
      case (l, r) if l.nonEmpty && r.nonEmpty && l.head == r.head => compress(l.tail, r.tail, common.append(l.head))
    }
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2).toArray
    val a = inputs(0)
    val b = inputs(1)

    val (common, al, bl) = compress(a, b, new StringBuilder())
    println(s"${common.length} ${common}")
    println(s"${al.length} ${al}")
    println(s"${bl.length} ${bl}")
  }
}
