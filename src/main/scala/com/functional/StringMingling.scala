package com.functional

object StringMingling {

  def mingle(p: String, s: String, out: StringBuilder): StringBuilder = {
    (p, s) match {
      case (a, b) if a.nonEmpty && b.nonEmpty => out.append(a.head).append(b.head); mingle(a.tail, b.tail, out)
      case (a, b) if a.isEmpty && b.isEmpty => out
    }
  }

  def main(args: Array[String]) = {
    val input = scala.io.Source.stdin.getLines().take(2).toArray
    val p = input(0)
    val s = input(1)

    val res = mingle(p, s, new StringBuilder).toString
    println(res)
  }
}
