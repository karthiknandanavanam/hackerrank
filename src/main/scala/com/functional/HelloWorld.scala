package com.functional

object HelloWorld {
  def f() = println("Hello World!")

  def main(args: Array[String]) = {
    f
  }
}
