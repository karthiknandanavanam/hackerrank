package com.functional

object SuperDigit {

  def superD(l: String): Int = {
    l match {
      case a if a.length == 1 => a.toInt
      case a => superD(a.map(c => c.asDigit).sum.toString)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine().split(" ")
    val n = input(0)
    val k = input(1).toInt

    val sum = n.map(c => c.asDigit).sum.toString
    val rSum = (0 until k).map(_ => sum).mkString("")
    val res = superD(rSum)
    println(res)
  }
}
