package com.functional

object FibonacciNumbers {

  def fibonacci(n: Int): Int = {
    n match {
      case 1 => 0
      case 2 => 1
      case a => fibonacci(a - 1) + fibonacci(a - 2)
    }
  }

  def main(args: Array[String]) = {
    val n = scala.io.Source.stdin.getLines().next().toInt
    val res = fibonacci(n)
    println(res)
  }
}
