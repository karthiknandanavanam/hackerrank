package com.functional

object FilterArray {

  def filter(limit: Int, arr: List[Int]): List[Int] = {
    def recur(list: List[Int], res: List[Int]): List[Int] = {
      list match {
        case a if a.isEmpty => res
        case a if a.nonEmpty && a.head < limit => recur(list.tail, res ++ List(list.head))
        case a if a.nonEmpty => recur(list.tail, res)
      }
    }
    recur(arr, List())
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val limit = scan.nextInt()
    var list = List[Int]()
    for (i <- 0 until 9) {
      val num = scan.nextInt()
      list = list ++ List(num)
    }

    val result = filter(limit, list)
    println(result.mkString("\n"))
  }
}
