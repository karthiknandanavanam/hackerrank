package com.functional

import scala.collection.mutable

object StringReductions {

  def reduce(in: String, out: mutable.LinkedHashSet[Char]): String = {
    (in, out) match {
      case (a, b) if a.isEmpty => out.mkString("")
      case (a, b) if a.nonEmpty && out.contains(a.head) => reduce(a.tail, out)
      case (a, b) if a.nonEmpty && !out.contains(a.head) => out.add(a.head); reduce(a.tail, out)
    }
  }

  def main(args: Array[String]) = {
    val input = scala.io.Source.stdin.getLines().next()
    val output = reduce(input, scala.collection.mutable.LinkedHashSet())
    println(output)
  }
}
