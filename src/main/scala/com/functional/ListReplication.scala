package com.functional

object ListReplication {
  def f(n: Int, arr: Array[Int]) = {
    def repeat(a: Int, r: Int): List[Int] = {
      r match {
        case 1 => List(a)
        case p => List(a) ++ repeat(a, r - 1)
      }
    }

    arr.flatMap(rec => repeat(rec, n)).toList
  }

  def main(args: Array[String]) = {
    val inputs = io.Source.stdin.getLines().take(5).toArray
    val size = inputs(0).toInt
    val arr = inputs.tail.map(rec => rec.toInt)

    println(f(size, arr).mkString(","))
  }
}
