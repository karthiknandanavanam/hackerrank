package com.functional

object HelloWorldNTimes {

  def f(n: Int): Unit = {
    n match {
      case 0 => println("")
      case a => {
        println("Hello World")
        f(a - 1)
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val num = scan.nextInt()
    f(num)
  }
}
