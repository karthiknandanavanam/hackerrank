package com.functional

object EvaluatingEX {

  def factorial(n: Long): Long = {
    n match {
      case 1 => 1
      case a => a * factorial(a - 1)
    }
  }

  def f(x: Double): Double = {
    def ePower(n: Long): Double = {
      n match {
        case 0 => 1
        case a => (Math.pow(x, a) / factorial(a)) + ePower(a - 1)
      }
    }
    ePower(9)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Double](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextDouble()
    }

    for (i <- 0 until size) {
      println("%.4f".format(f(arr(i))))
    }
  }
}
