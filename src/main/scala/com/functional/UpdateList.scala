package com.functional

object UpdateList {

  def f(arr: List[Int]): List[Int] = {
    arr match {
      case a if a.isEmpty => List()
      case a if a.head < 0 => a.head * -1 :: f(arr.tail)
      case a if a.head >= 0 => a.head :: f(arr.tail)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val res = f(arr.toList)
    println(res.mkString(" "))
  }
}
