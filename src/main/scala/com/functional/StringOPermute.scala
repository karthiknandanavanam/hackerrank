package com.functional

object StringOPermute {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt
    val strs = (0 until size).map(_ => scan.nextLine).toArray

    def swap(i: Int, input: Array[Char]): Unit = {
      (i, input) match {
        case (a, _) if a < input.length => val temp = input(i); input(i) = input(i + 1); input(i + 1) = temp; swap(i + 2, input)
        case (_, _) =>
      }
    }

    strs foreach { str =>
      val input = str.toCharArray
      swap(0, input)
      println(input.mkString(""))
    }
  }
}
