package com.functional

object FilterPositionsInAList {

  def f(arr: List[Int]): List[Int] = {
    def recur(arr: List[Int], count: Int): List[Int] = {
      (count, arr) match {
        case (_, b) if b.isEmpty => List()
        case (a, _) if a % 2 != 0 => recur(arr.tail, count + 1)
        case (a, _) if a % 2 == 0 => arr.head :: recur(arr.tail, count + 1)
      }
    }
    recur(arr, 1)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val res = f(arr.toList)
    println(res.mkString(" "))
  }
}
