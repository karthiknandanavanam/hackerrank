package com.functional

object SumOfOddElements {

  def f(arr: List[Int]): Int = {
    arr match {
      case a if a.isEmpty => 0
      case a if a.head % 2 == 0 => f(arr.tail)
      case a if a.head % 2 != 0 => a.head + f(arr.tail)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val res = f(arr.toList)
    println(res)
  }
}
