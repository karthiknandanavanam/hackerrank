package com.functional

object ArrayOfNElements {

  def f(n: Int) = {
    def recur(count: Int): List[Int] = {
      count match {
        case a if a == n => List(a)
        case _ => count :: recur(count + 1)
      }
    }
    recur(1)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val num = scan.nextInt()
    val res = f(num)
    println(res.mkString("[", ", ", "]"))
  }
}
