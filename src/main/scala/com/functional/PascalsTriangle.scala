package com.functional

object PascalsTriangle {

  def factorial(n: Int): Int = {
    n match {
      case 0 => 1
      case 1 => 1
      case a => a * factorial(a - 1)
    }
  }

  def comb(n: Int, r: Int) = factorial(n) / (factorial(r) * factorial(n - r))

  def pascal(n: Int) = {
    (0 to n).map(i => comb(n, i)).mkString(" ")
  }

  def pascalLines(n: Int) = {
    (0 until n).map(i => pascal(i)).mkString("\n")
  }

  def main(args: Array[String]) = {
    val size = scala.io.Source.stdin.getLines().next().toInt
    val res = pascalLines(size)
    println(res)
  }
}
