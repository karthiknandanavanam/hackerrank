package com.statistics

object MeanMedianMode {

  def mean(arr: Array[Int]) = {
    var sum = 0
    for (i <- 0 until arr.length) {
      sum = sum + arr(i)
    }
    sum * 1.0 / arr.length
  }

  def median(arr: Array[Int]) = {
    val size = arr.length
    val sorted = arr.sortWith((a, b) => a < b)

    if (size == 1) {
      sorted(0).toDouble
    }
    else if (size % 2 == 0) {
      (sorted(size / 2) + sorted(size / 2 - 1)) / 2.0
    }
    else {
      sorted(Math.ceil(size / 2.0).toInt).toDouble
    }
  }

  def mode(arr: Array[Int]): Int = {
    val sorted = arr.sortWith((a, b) => a < b)
    val map = sorted
      .groupBy(a => a)
      .map(rec => (rec._1, rec._2.length))

    val maxCount = map.map(rec => rec._2).max

    sorted.map(num => (num, map(num)))
      .filter(pair => pair._2 == maxCount)
      .map(pair => pair._1)
      .min
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.next().toInt

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      val value = scan.next().toInt
      arr(i) = value
    }

    val meanValue = mean(arr)
    val medianValue = median(arr)
    val modeValue = mode(arr)

    println("%.1f".format(meanValue).toDouble)
    println("%.1f".format(medianValue).toDouble)
    println(modeValue)
  }
}
