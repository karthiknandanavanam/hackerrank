package com.statistics

object InterquartileRange {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt

    val vals = new Array[Int](size)
    for (i <- 0 until size) {
      vals(i) = scan.next().toInt
    }

    val weights = new Array[Int](size)
    for (i <- 0 until size) {
      weights(i) = scan.next().toInt
    }

    val fullList = new Array[Int](weights.sum)

    var index = 0
    for (i <- 0 until size) {
      val value = vals(i)
      val weight = weights(i)
      for (j <- 0 until weight) {
        fullList(index) = value
        index += 1
      }
    }

    val sorted = fullList.sortWith((a, b) => a < b)
    val half = fullList.length / 2

    if (sorted.length % 2 != 0 && half % 2 == 0) {
      val left = (0 + half - 1) / 2
      val right = (half + 1 + fullList.length - 1) / 2
      val leftQuartile = (sorted(left) + sorted(left + 1)) / 2.0
      val rightQuartile = (sorted(right) + sorted(right + 1)) / 2.0
      println(rightQuartile - leftQuartile)
    }
    else if (sorted.length % 2 == 0 && half % 2 != 0) {
      val left = (0 + half - 1) / 2
      val right = (half + fullList.length - 1) / 2
      println("%.1f".format((sorted(right) - sorted(left)).toFloat))
    }
    else if (sorted.length % 2 != 0 && half % 2 != 0) {
      val left = (0 + half - 1) / 2
      val right = (half + 1 + fullList.length - 1) / 2
      println("%.1f".format((sorted(right) - sorted(left)).toFloat))
    }
    else if (sorted.length % 2 == 0 && half % 2 == 0) {
      val left = (0 + half - 1) / 2
      val right = (half + 1 + fullList.length - 1) / 2
      val leftQuartile = (sorted(left) + sorted(left + 1)) / 2.0
      val rightQuartile = (sorted(right) + sorted(right + 1)) / 2.0
      println(rightQuartile - leftQuartile)
    }
  }
}
