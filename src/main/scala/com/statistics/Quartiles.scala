package com.statistics

object Quartiles {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt
    val array = new Array[Int](size)
    for (i <- 0 until size) {
      array(i) = scan.next().toInt
    }

    val sort = array.sortWith((a, b) => a < b)

    if (size % 2 != 0) {
      val medianIndex = size / 2
      val leftIndex = (0 + medianIndex - 1) / 2
      val rightIndex = (medianIndex + 1 + size - 1) / 2

      val left = (sort(leftIndex) + sort(leftIndex + 1)) / 2.0
      val mid = sort(medianIndex)
      val right = (sort(rightIndex) + sort(rightIndex + 1)) / 2.0

      if (medianIndex % 2 == 0) {
        val left = (sort(leftIndex) + sort(leftIndex + 1)) / 2.0
        val right = (sort(rightIndex) + sort(rightIndex + 1)) / 2.0
        if (left % 1 == 0) println(left.toInt) else println(left)
        if (mid % 1 == 0) println(mid.toInt) else println(mid)
        if (right % 1 == 0) println(right.toInt) else println(right)
      }
      else {
        val left = sort(leftIndex)
        val right = sort(rightIndex)
        if (left % 1 == 0) println(left.toInt) else println(left)
        if (mid % 1 == 0) println(mid.toInt) else println(mid)
        if (right % 1 == 0) println(right.toInt) else println(right)
      }
    }
    else {
      val medianIndex = size / 2
      val leftIndex = (0 + medianIndex - 1) / 2
      val rightIndex = (medianIndex + size - 1) / 2

      val mid = (sort(medianIndex) + sort(medianIndex - 1)) / 2.0

      if (medianIndex % 2 == 0) {
        val left = (sort(leftIndex) + sort(leftIndex + 1)) / 2.0
        val right = (sort(rightIndex) + sort(rightIndex + 1)) / 2.0
        if (left % 1 == 0) println(left.toInt) else println(left)
        if (mid % 1 == 0) println(mid.toInt) else println(mid)
        if (right % 1 == 0) println(right.toInt) else println(right)
      }
      else {
        val left = sort(leftIndex)
        val right = sort(rightIndex)
        if (left % 1 == 0) println(left.toInt) else println(left)
        if (mid % 1 == 0) println(mid.toInt) else println(mid)
        if (right % 1 == 0) println(right.toInt) else println(right)
      }
    }
  }
}
