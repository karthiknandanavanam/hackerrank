package com.statistics
import java.util.Scanner

object WeightedMean {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.next().toInt

    val arr = createArray(scan, size)
    val weights = createArray(scan, size)

    val wSum = weightedSum(arr, weights)
    val weightSum = sum(weights)

    val weightedMean = 1.0 * wSum / weightSum
    println("%.1f".format(weightedMean).toFloat)
  }

  def weightedSum(arr: Array[Int], weights: Array[Int]) = {
    var sum = 0
    for (i <- 0 until arr.size) {
      sum = sum + (arr(i) * weights(i))
    }
    sum
  }

  def sum(arr: Array[Int]) = {
    var sum = 0
    for (i <- 0 until arr.length) {
      sum = sum + arr(i)
    }
    sum
  }

  private def createArray(scan: java.util.Scanner, size: Int) = {
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.next().toInt
    }
    arr
  }
}
