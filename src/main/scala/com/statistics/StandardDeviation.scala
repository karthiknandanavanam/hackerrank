package com.statistics

object StandardDeviation {
  def findMean(nums: Array[Int]) = {
    var sum = 0
    for(i <- 0 until nums.length) {
      sum = sum + nums(i)
    }
    1.0 * sum / nums.length
  }

  def findSD(nums: Array[Int]) = {
    val mean = findMean(nums)
    var sumOfSquares = 0.0
    for (i <- 0 until nums.length) {
      sumOfSquares = sumOfSquares + Math.pow((nums(i) - mean) * 1.0, 2.0)
    }
    val variance = sumOfSquares / nums.length
    val sd = Math.sqrt(variance)
    sd
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val sd: Double = findSD(arr)
    println("%.1f".format(sd))
  }
}
