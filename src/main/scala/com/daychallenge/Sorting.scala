package com.daychallenge

object Sorting {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var swap = 0
    for (i <- 0 until size) {
      for (j <- 0 until size - i - 1) {
        if (arr(j) > arr(j + 1)) {
          val temp = arr(j)
          arr(j) = arr(j + 1)
          arr(j + 1) = temp
          swap = swap + 1
        }
      }
    }

    println("Array is sorted in " + swap + " swaps.")
    println("First Element: " + arr(0))
    println("Last Element: " + arr(size - 1))
  }
}
