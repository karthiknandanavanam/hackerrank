package com.daychallenge

object BinarySearchTrees {

  trait Tree {
    def addLeft(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le = node
      }
    }
    def addRight(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri = node
      }
    }
    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.num
      }
    }
    def left: Tree = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le
      }
    }
    def right: Tree = {
      this match  {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri
      }
    }
    def isEmpty: Boolean = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }
    def display: String = {
      var res = ""
      def rec(node: Tree): Unit = {
        node match {
          case a: Empty => res = res
          case a: NonEmpty => {
            res = res + a.data
            rec(a.left)
            rec(a.right)
          }
        }
      }
      rec(this)
      res
    }

    def getHeight() = {
      var maxHeight = 0
      def rec(node: Tree, h: Int): Unit = {
        node match {
          case a: Empty => if (maxHeight < h) maxHeight = h
          case a: NonEmpty => {
            rec(a.left, h + 1)
            rec(a.right, h + 1)
          }
        }
      }
      rec(this, -1)
      maxHeight
    }

    def insert(a: Int) = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          var iter = this
          while (iter != null) {
            if (node.data < iter.data && iter.left.isEmpty) {
              iter.addLeft(node)
              iter = null
            }
            else if (node.data > iter.data && iter.right.isEmpty) {
              iter.addRight(node)
              iter = null
            }
            else if (node.data < iter.data) {
              iter = iter.left
            }
            else if (node.data > iter.data) {
              iter = iter.right
            }
            else {
              iter = null
            }
          }
          this
        }
      }
    }
  }

  class NonEmpty(a: Int) extends Tree {
    val num = a
    var le: Tree = new Empty
    var ri: Tree = new Empty
  }

  class Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var bst: Tree = new Empty
    arr.foreach {
      rec => bst = bst.insert(rec)
    }

    println(bst.display)
    println(bst.getHeight)
  }
}
