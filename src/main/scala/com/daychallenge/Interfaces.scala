package com.daychallenge

object Interfaces {

  trait AdvancedArithmetic {
    def divisionSum(n: Int): Int
  }

  class Calculator extends AdvancedArithmetic {
    override def divisionSum(n: Int): Int = {
      println("I implemented: AdvancedArithmetic")
      var i = 1
      var sum = 0
      while (i <= n) {
        if (n % i == 0) {
          sum = sum + i
        }
        i = i + 1
      }
      sum
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val num = scan.nextInt()
    val cal = new Calculator
    println(cal.divisionSum(num))
  }
}
