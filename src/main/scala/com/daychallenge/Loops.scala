package com.daychallenge

object Loops {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val n = scan.next().toInt

    for (i <- 1 to 10) {
      println(n + " x " + i + " = " + (n * i))
    }
  }
}
