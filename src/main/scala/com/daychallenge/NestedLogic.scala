package com.daychallenge

object NestedLogic {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val day = scan.nextInt()
    val month = scan.nextInt()
    val year = scan.nextInt()

    val eDay = scan.nextInt()
    val eMonth = scan.nextInt()
    val eYear = scan.nextInt()

    val dayD = day - eDay
    val monthD = month - eMonth
    val yearD = year - eYear

    if (yearD >= 1) {
      println("10000")
    }
    else if (monthD >= 1) {
      if (yearD == 0) {
        println(monthD * 500)
      }
      else {
        println("0")
      }
    }
    else if (dayD >= 1) {
      if (yearD == 0 && monthD == 0) {
        println(dayD * 15)
      }
      else {
        println("0")
      }
    }
    else {
      println("0")
    }
  }
}
