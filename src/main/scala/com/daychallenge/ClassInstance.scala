package com.daychallenge

class Person {
  var age: Int = 0

  def this(age: Int) = {
    this()
    if (age < 0) {
      println("Age is not valid, setting age to 0.")
      this.age = 0
    }
    else {
      this.age = age
    }
  }

  def yearPasses() = {
    this.age = this.age + 1
  }

  def amIOld() = {
    if (age < 13) {
      println("You are young.")
    }
    else if (age >= 13 && age < 18) {
      println("You are a teenager.")
    }
    else {
      println("You are old.")
    }
  }
}

object ClassInstance {
  def main(args: Array[String]) = {
    val p1 = new Person(-1)
    p1.yearPasses()
    p1.amIOld()

    val p2 = new Person(13)
    p1.yearPasses()
    p1.amIOld()
  }
}
