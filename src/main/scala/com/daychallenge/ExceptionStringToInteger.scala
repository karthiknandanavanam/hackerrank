package com.daychallenge

object ExceptionStringToInteger {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val res = scan.next()
    try {
      println(res.toInt)
    }
    catch {
      case a: NumberFormatException => println("Bad String")
    }
  }
}
