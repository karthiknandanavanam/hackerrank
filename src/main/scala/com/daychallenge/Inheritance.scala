package com.daychallenge

object Inheritance {
  class Person(firstName: String, lastName: String, id: Int) {
    override def toString(): String = {
      val s = new StringBuilder
      s.append("Name: ").append(firstName).append(", ").append(lastName).append("\n")
        .append("ID: ").append(id)
      s.toString
    }
  }

  class Student(firstName: String, lastName: String, id: Int, scores: Array[Int]) extends Person(firstName, lastName, id) {

    def calculate(): Char = {
      val score = scores.sum
      val num = scores.length
      val avg = 1.0 * score / num
      avg match {
        case a if 90 <= a && a <= 100 => 'O'
        case a if 80 <= a && a < 90 => 'E'
        case a if 70 <= a && a < 80 => 'A'
        case a if 55 <= a && a < 70 => 'P'
        case a if 40 <= a && a < 55 => 'D'
        case a if a < 40 => 'T'
      }
    }

    override def toString(): String = {
      val s = new StringBuilder
      s.append("Name: ").append(firstName).append(", ").append(lastName).append("\n")
        .append("ID: ").append(id).append("\n")
        .append("Grade: ").append(this.calculate())
      s.toString
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val firstName = scan.next()
    val lastName = scan.next()
    val id = scan.nextInt()
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      val num = scan.nextInt()
      arr(i) = num
    }

    val s = new Student(firstName, lastName, id, arr)
    println(s.toString())
  }
}
