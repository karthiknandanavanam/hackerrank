package com.daychallenge

object MoreExceptions {

  def power(a: Int, n: Int) = {
    if (a < 0 || n < 0) {
      throw new IllegalArgumentException("n and p should be non-negative")
    }
    var ans = 1
    for (i <- 0 until n) {
      ans = ans * a
    }
    ans
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[(Int, Int)](size)
    for (i <- 0 until size) {
      arr(i) = (scan.nextInt(), scan.nextInt())
    }

    for (i <- 0 until size) {
      try {
        val res = power(arr(i)._1, arr(i)._2)
        println(res)
      }
      catch {
        case a: IllegalArgumentException => println(a.getMessage)
      }
    }
  }
}
