package com.daychallenge

object BinaryNumbers {

  def convertToBinary(num: Int) = {
    val binSize = Math.ceil(Math.log(num)/Math.log(2)).toInt + 1
    val binArr = new Array[Short](binSize)

    var div = num
    var i = binSize - 1
    while (div != 0) {
      val rem = div % 2
      binArr(i) = rem.toShort
      i = i - 1
      div = div / 2
    }
    binArr
  }

  def findMaxOnes(bin: Array[Short]) = {
    var max = 0
    var count = 0
    for (i <- 0 until bin.size) {
      bin(i) match {
        case 1 => count = count + 1
        case 0 => count = 0
      }

      if (max < count) {
        max = count
      }
    }
    max
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val num = scan.nextInt()

    val bin = convertToBinary(num)
    val max = findMaxOnes(bin)

    println(max)
  }
}
