package com.daychallenge

object DataTypes {
  def main(args: Array[String]) = {
    val i = 4
    val d = 4.0
    val s = "HackerRank "

    val variables = scala.io.Source.stdin.getLines().take(3)
    val int = variables.next().toInt
    val double = variables.next().toDouble
    val string = variables.next().toString

    println(i + int)
    println(d + double)
    println(s + string)
  }
}
