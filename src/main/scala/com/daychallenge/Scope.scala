package com.daychallenge

object Scope {
  class Difference(arr: Array[Int]) {
    var maximumDifference: Int = 0

    def computeDifference() = {
      maximumDifference = Math.abs(arr.max - arr.min)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val dif = new Difference(arr)
    dif.computeDifference()
    println(dif.maximumDifference)
  }
}
