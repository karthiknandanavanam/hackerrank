package com.daychallenge

object Conditional {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val num = scan.next().toInt
    if (num % 2 != 0) {
      println("Weird")
    }
    else if (num % 2 == 0 && Range(2, 6).contains(num)) {
      println("Not Weird")
    }
    else if (num % 2 == 0 && Range(6, 21).contains(num)) {
      println("Weird")
    }
    else if (num % 2 == 0 && num > 20) {
      println("Not Weird")
    }
  }
}
