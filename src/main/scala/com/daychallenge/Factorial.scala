package com.daychallenge

object Factorial {

  def factorial(n: Int): Int = {
    n match {
      case 1 => 1
      case n => n * factorial(n - 1)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val num = scan.nextInt()
    println(factorial(num))
  }
}
