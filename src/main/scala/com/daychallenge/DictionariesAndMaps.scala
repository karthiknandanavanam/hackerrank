package com.daychallenge

object DictionariesAndMaps {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    var map = Map[String, String]()
    for (i <- 0 until size) {
      val name = scan.next()
      val phone = scan.next()
      map = map + (name -> phone)
    }

    val notFound = "Not found"
    for (i <- 0 until size) {
      val query = scan.next()

      val res = map.getOrElse(query, notFound)
      if (res == notFound) println(notFound) else println(query + "=" + res)
    }
  }
}
