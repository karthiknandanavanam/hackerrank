package com.daychallenge

object AbstractClasses {
  abstract class Book(title: String, author: String) {
    def display(): String
  }

  class MyBook(title: String, author: String, price: Int) extends Book(title, author) {
    override def display(): String = {
      val s = new StringBuilder
      s.append("Title: ").append(title).append("\n")
        .append("Author: ").append(author).append("\n")
        .append("Price: ").append(price).append("\n")
      s.toString()
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val title = scan.nextLine()
    val author = scan.nextLine()
    val price = scan.nextInt()

    val myBook = new MyBook(title, author, price)
    println(myBook.display())
  }
}
