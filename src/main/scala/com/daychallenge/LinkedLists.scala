package com.daychallenge

object LinkedLists {
  trait LinkedList {
    def value(): Int
    def link(): LinkedList
    def updateLink(a: LinkedList)
    def isEmpty: Boolean
    def display: String = {
      val sb = new StringBuilder
      var iter = this
      while (!iter.isEmpty) {
        sb.append(iter.value())
        sb.append(" ")
        iter = iter.link
      }
      sb.toString
    }
    def addToEnd(a: Int): LinkedList = {
      val node =  new NonEmpty(a)
      if (this.isEmpty) {
        node
      }
      else {
        var prev = this
        var iter = this.link
        while (!iter.isEmpty) {
          prev = iter
          iter = iter.link
        }
        prev.updateLink(node)
        this
      }
    }
  }

  class NonEmpty(a: Int) extends LinkedList {
    var next: LinkedList = new Empty
    override def isEmpty: Boolean = false
    override def value(): Int = a
    override def link(): LinkedList = next
    override def updateLink(a: LinkedList): Unit = this.next = a
  }

  class Empty extends LinkedList {
    override def isEmpty: Boolean = true
    override def value(): Int = throw new UnsupportedOperationException
    override def link(): LinkedList = throw new UnsupportedOperationException
    override def updateLink(a: LinkedList) = throw new UnsupportedOperationException
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    var ll: LinkedList = new Empty
    for (i <- 0 until size) {
      val num = scan.nextInt()
      ll = ll.addToEnd(num)
    }
    println(ll.display)
  }
}
