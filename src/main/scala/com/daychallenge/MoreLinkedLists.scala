package com.daychallenge

object MoreLinkedLists {

  trait LinkedList {
    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.data
      }
    }

    def isEmpty: Boolean = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def next: LinkedList = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ne
      }
    }

    def addNext(node: LinkedList) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ne = node
      }
    }

    def insert(a: Int) = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          if (a.ne.isEmpty) {
            a.addNext(node)
            a.ls = node
          }
          else {
            a.ls.addNext(node)
            a.ls = node
          }
          this
        }
      }
    }

    def removeDuplicates: LinkedList = {
      this match {
        case a: Empty => a
        case a: NonEmpty if a.next.isEmpty => a
        case a: NonEmpty => {
          var prev = this
          var iter = this.next
          while (!iter.isEmpty) {
            if (prev.data == iter.data) {
              iter = iter.next
            }
            else if (prev.data != iter.data) {
              prev.addNext(iter)
              prev = prev.next
              iter = iter.next
            }
          }
          prev.addNext(iter)
          this.asInstanceOf[NonEmpty].ls = prev
          this
        }
      }
    }

    def display: String = {
      var res = ""
      var iter = this
      while (!iter.isEmpty) {
        res = res + iter.data + " "
        iter = iter.next
      }
      res
    }
  }

  class NonEmpty(a: Int) extends LinkedList {
    var ne: LinkedList = new Empty
    var ls: LinkedList = new Empty
    override def data: Int = a
  }

  class Empty extends LinkedList {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    var ll: LinkedList = new Empty
    for (i <- 0 until size) {
      val num = scan.nextInt()
      ll = ll.insert(num)
    }

    println(ll.display)
    println(ll.removeDuplicates.display)
  }
}
