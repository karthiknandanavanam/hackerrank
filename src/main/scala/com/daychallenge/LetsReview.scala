package com.daychallenge

object LetsReview {
  def convertToOddEven(str: String) = {
    var even = ""
    var odd = ""
    for (i <- 0 until str.length) {
      if (i % 2 == 0) {
        even = even + str(i)
      }
      else {
        odd = odd + str(i)
      }
    }
    even + " " + odd
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.next().toInt

    val arr = new Array[String](size)
    for (i <- 0 until size) {
      val str = scan.next()
      val conv = convertToOddEven(str)
      arr(i) = conv
    }
    println(arr.mkString("\n"))
  }
}
