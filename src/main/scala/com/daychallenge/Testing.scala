package com.daychallenge

object Testing {

  def main(args: Array[String]) = {
    println("5")
    println("3 3")
    println("-1 0 1")
    println("5 3")
    println("0 -1 -2 1 4")
    println("7 4")
    println("-1 -3 0 4 2 5 6")
    println("8 2")
    println("0 -1 2 1 4 5 6 7")
    println("9 5")
    println("-1 -3 4 -2 0 5 6 7 2")

    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val arr = new Array[String](size)
    for (i <- 0 until size) {
      val n = scan.nextInt()
      val k = scan.nextInt()
      var count = 0
      for (j <- 0 until n) {
        val num = scan.nextInt()
        if (num <= 0) {
          count = count + 1
        }
      }
      if (count >= k) {
        arr(i) = "NO"
      }
      else {
        arr(i) = "YES"
      }
    }

    println(arr.mkString("\n"))
  }
}
