package com.daychallenge

object BitwiseAnd {

  def maxAnd(num: Int, k: Int) = {
    var max = Int.MinValue
    val arr = (1 to num)

    for (i <- 0 until num) {
      for (j <- i + 1 until num) {
        val res = arr(i) & arr(j)
        if (max < res && res < k) {
          max = res
        }
      }
    }
    max match {
      case Int.MinValue => 0
      case a => a
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[(Int, Int)](size)
    for (i <- 0 until size) {
      arr(i) = (scan.nextInt(), scan.nextInt())
    }

    for (i <- 0 until size) {
      val num = arr(i)._1
      val k = arr(i)._2
      val res = maxAnd(num, k)
      println(res)
    }
  }
}
