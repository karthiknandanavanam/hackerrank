package com.daychallenge

object RegexPatterns {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt
    val arr = new Array[(String, String)](size)
    for (i <- 0 until size) {
      val row = scan.nextLine().split(" ")
      arr(i) = (row(0), row(1))
    }

    var names = List[String]()
    val reg = """^.*@gmail.com$"""
    for (i <- 0 until size) {
      val (name, email) = arr(i)
      if (email.matches(reg)) {
        names = names ++ List(name)
      }
    }
    println(names.sortWith((a, b) => a < b).mkString("\n"))
  }
}
