package com.daychallenge

object RunningTimeAndComplexity {

  def isPrime(i: Long): Boolean = {
    if (i < 2) {
      false
    }
    else if (i == 2) {
      true
    }
    else {
      val size = Math.sqrt(i).toLong
      var iter = 2L
      while (iter <= size) {
        if (i % iter == 0) {
          return false
        }
        iter = iter + 1
      }
      true
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Long](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextLong()
    }

    for (i <- 0 until size) {
      if (isPrime(arr(i))) {
        println("Prime")
      }
      else {
        println("Not prime")
      }
    }
  }

}
