package com.daychallenge

import scala.collection.mutable

object BSTLevelOrderTraversal {
  trait Tree {
    def isEmpty: Boolean = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.data
      }
    }

    def addLeft(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le = node
      }
    }

    def addRight(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri = node
      }
    }

    def left: Tree = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.le
      }
    }

    def right: Tree = {
      this match {
        case a: Empty => throw new UnsupportedOperationException
        case a: NonEmpty => a.ri
      }
    }

    def insert(a: Int) = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          var iter = this
          while (!iter.isEmpty) {
            if (node.data < iter.data && iter.left.isEmpty) {
              iter.addLeft(node)
              iter = new Empty
            }
            else if (node.data > iter.data && iter.right.isEmpty) {
              iter.addRight(node)
              iter = new Empty
            }
            else if (node.data < iter.data) {
              iter = iter.left
            }
            else if (node.data > iter.data) {
              iter = iter.right
            }
          }
          this
        }
      }
    }

    def levelOrder = {
      var queue = mutable.Queue[Tree](this)

      var res = ""

      while (queue.nonEmpty) {
        queue match {
          case a if a.isEmpty => queue = queue
          case a if !a.isEmpty => {
            val node = a.dequeue()
            res = res + node.data
            if (!node.left.isEmpty) queue.enqueue(node.left)
            if (!node.right.isEmpty) queue.enqueue(node.right)
          }
        }
      }
      res
    }
  }

  class NonEmpty(a: Int) extends Tree {
    var le: Tree = new Empty
    var ri: Tree = new Empty
    override def data = a
  }

  class Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var bst: Tree = new Empty
    arr.foreach {
      rec => bst = bst.insert(rec)
    }

    println(bst.levelOrder)
  }
}
