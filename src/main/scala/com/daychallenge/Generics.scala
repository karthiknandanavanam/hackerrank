package com.daychallenge

object Generics {

  class PrintArray[T] {
    def print(arr: Array[T]) = {
      for (a <- arr) {
        println(a)
      }
    }
  }

  def main(args: Array[String]) = {
    val arrString = new Array[String](10)
    var a = 65
    for (i <- 0 until 10) {
      arrString(i) = a.toChar.toString
      a = a + 1
    }

    val arrInt = new Array[Int](10)
    var b = 65
    for (i <- 0 until 10) {
      arrInt(i) = b
      b = b + 1
    }

    val printString = new PrintArray[String]
    printString.print(arrString)

    val printInt = new PrintArray[Int]
    printInt.print(arrInt)
  }
}
