package com.daychallenge

import scala.collection.mutable

object QueuesAndStacks {
  trait LinkedList {
    def data(): Int
    def isEmpty(): Boolean
    def next(): LinkedList
    def addLink(a: LinkedList)
    def addBegin(a: LinkedList): LinkedList = {
      this match {
        case b: Empty => a
        case b: NonEmpty => a.addLink(b); a
      }
    }
    def addEnd(a: LinkedList): LinkedList = {
      this match {
        case b: Empty => a
        case b: NonEmpty => b.end.addLink(a); b.end = a; this
      }
    }
    def display() = {
      var res = ""
      this match {
        case a if a.isEmpty() => ""
        case a => {
          var iter = this
          while (!iter.isEmpty()) {
            res = res + iter.data() + " "
            iter = iter.next()
          }
        }
      }
      res
    }
  }

  class NonEmpty(d: Int) extends LinkedList {
    var end: LinkedList = this
    var ne: LinkedList = new Empty
    override def data(): Int = d
    override def isEmpty(): Boolean = false
    override def next(): LinkedList = ne
    override def addLink(a: LinkedList): Unit = ne = a
  }

  class Empty extends LinkedList {
    override def data(): Int = throw new UnsupportedOperationException
    override def isEmpty(): Boolean = true
    override def next(): LinkedList = throw new UnsupportedOperationException
    override def addLink(a: LinkedList): Unit = throw new UnsupportedOperationException
  }

  class Queue {
    var ll: LinkedList = new Empty

    def enqueue(a: Int) = {
      val node = new NonEmpty(a)
      ll = ll.addEnd(node)
    }

    def dequeue(): Int = {
      ll match {
        case b if b.isEmpty() => throw new UnsupportedOperationException
        case b => val res = b.data; ll = ll.next(); res
      }
    }

    def peek() = {
      ll match {
        case ll: Empty => new UnsupportedOperationException
        case ll: NonEmpty => ll.data()
      }
    }

    def display() = {
      ll.display()
    }
  }

  class Stack {
    var ll: LinkedList = new Empty

    def push(a: Int) = {
      val node = new NonEmpty(a)
      ll = ll.addBegin(node)
    }

    def pop() = {
      ll match {
        case a: Empty => new UnsupportedOperationException
        case a: NonEmpty => val res = ll.data(); ll = ll.next(); res
      }
    }

    def display() = {
      ll.display()
    }
  }

  def isPalindrome(string: String): String = {
    val stack = mutable.Stack[Char]()
    val queue = mutable.Queue[Char]()
    var iter = string
    while (!iter.isEmpty) {
      val char = iter.head
      stack.push(char)
      queue.enqueue(char)
      iter = iter.tail
    }

    while (!stack.isEmpty && !queue.isEmpty) {
      val sChar = stack.pop()
      val qChar = queue.dequeue()
      if (sChar != qChar) {
        return "The word, " + string + ", is not a palindrome."
      }
    }
    return "The word, " + string + ", is a palindrome."
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val string = scan.next()

    println(isPalindrome(string))
  }
}
