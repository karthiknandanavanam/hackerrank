package com.daychallenge

object TwoDArrays {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val arr = Array.ofDim[Int](6, 6)
    for(i <- 0 until 6) {
      for (j <- 0 until 6) {
        arr(i)(j) = scan.nextInt()
      }
    }

    var max = Int.MinValue
    for (i <- 0 to 3) {
      for (j <- 0 to 3) {
        val top = arr(i)(j) + arr(i)(j + 1) + arr(i)(j + 2)
        val mid = arr(i + 1)(j + 1)
        val bottom = arr(i + 2)(j) + arr(i + 2)(j + 1) + arr(i + 2)(j + 2)
        val all = top + mid + bottom
        if (max <= all) {
          max = all
        }
      }
    }
    println(max)
  }
}
