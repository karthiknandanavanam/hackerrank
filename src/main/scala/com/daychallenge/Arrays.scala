package com.daychallenge

object Arrays {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    for(i <- (size - 1) to 0 by -1) {
      print(arr(i))
      print(" ")
    }
  }
}
