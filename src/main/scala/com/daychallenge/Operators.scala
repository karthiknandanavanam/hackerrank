package com.daychallenge

object Operators {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val mealCost = scan.next().toDouble
    val tipPercent = scan.next().toDouble
    val taxPercent = scan.next().toDouble

    val costWithTip = mealCost * (100 + tipPercent) / 100
    val tax = mealCost * taxPercent / 100

    println("The total meal cost is " + Math.round(costWithTip + tax) + " dollars.")
  }
}
