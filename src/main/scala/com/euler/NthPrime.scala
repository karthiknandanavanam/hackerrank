package com.euler

object NthPrime {
  val batchSize = 400000

  def size(arr: List[Array[Int]]): Int = {
    arr.map(rec => rec.length).sum
  }

  def createArr(num: Int): Array[Int] = {
    val arr = new Array[Int](batchSize)
    for (i <- 0 until batchSize) {
      arr(i) = num + i
    }
    arr
  }

  def seive(arr: Array[Int]) = {
    for (i <- 0 until arr.length) {
      if (arr(i) == 1) {
        arr(i) = 0
      }
      else if (arr(i) == 0) {
        arr(i) = 0
      }
      else {
        var prime = arr(i)
        var iter = i + prime
        while (iter < arr.length) {
          arr(iter) = 0
          iter = iter + prime
        }
      }
    }
  }

  def nthPrime(arr: List[Array[Int]], i: Int) = {
    arr.flatten.take(i)(i - 1)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val queries = new Array[Int](size)
    for (i <- 0 until size) {
      queries(i) = scan.nextInt()
    }

    var arr = List[Array[Int]]()
    var iter = 0
    val primes = createArr(iter)
    seive(primes)
    iter = iter + batchSize
    val onlyPrimes = primes.filter(rec => rec != 0)
    arr = (onlyPrimes :: arr.reverse).reverse

    for (i <- 0 until size) {
      println(nthPrime(arr, queries(i)))
    }
  }
}
