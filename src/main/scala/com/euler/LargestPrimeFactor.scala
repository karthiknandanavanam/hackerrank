package com.euler

object LargestPrimeFactor {

  def factor(num: Long): Long = {
    var i = 5L
    while (i <= Math.sqrt(num) + 1) {
      if (num % i == 0) {
        return i
      }
      i = i + 2
    }
    return -1
  }

  def findFactors(num: Long) = {
    var factors = Set[Long]()

    var number = num
    while (number != 1) {
      if (number % 2 == 0) {
        number = number / 2
        factors = factors + 2
      }
      else if (number % 3 == 0) {
        number = number / 3
        factors = factors + 3
      }
      else {
        val primeFactor = factor(number)
        if (primeFactor < 0) {
          factors = factors + number
          number = 1
        }
        else {
          factors = factors + primeFactor
          number = number / primeFactor
        }
      }
    }
    factors
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt

    val res = new Array[Long](size)
    for (i <- 0 until size) {
      val factors = findFactors(scan.next().toLong)
      res(i) = factors.max
    }
    println(res.mkString("\n"))
  }
}
