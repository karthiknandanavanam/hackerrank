package com.euler

object LargestPalindrome {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var palindromes = List[Int]()
    for (i <- 100 * 100 to 999 * 999) {
      val forward = i.toString
      val reverse = i.toString.reverse
      if (forward.equals(reverse) && isDivisibleByTwo3Digits(i)) {
        palindromes = i :: palindromes
      }
    }

    for (i <- 0 until size) {
      var index = 0
      var start = palindromes(0)
      while (start >= arr(i)) {
        index += 1
        start = palindromes(index)
      }
      println(start)
    }
  }

  def isDivisibleByTwo3Digits(n: Int): Boolean = {
    for (i <- 100 to 999) {
      val rem = n % i
      if (rem == 0 && (n / i).toString.length == 3) {
        return true
      }
    }

    return false
  }
}
