package com.euler

object SumSquareDifference {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val num = scan.nextInt()

    for (i <- 1 to num) {
      val n = scan.nextLong()
      val sumOfNumbersSquare = Math.pow(n * (n + 1) / 2, 2)
      val sumOfSquares = n * (n + 1) * (2 * n + 1) / 6

      val res = sumOfNumbersSquare - sumOfSquares
      println(res.toLong)
    }
  }
}
