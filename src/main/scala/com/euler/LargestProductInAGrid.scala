package com.euler

object LargestProductInAGrid {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val arr = Array.ofDim[Long](20, 20)
    for (i <- 0 until 20) {
      for (j <- 0 until 20) {
        arr(i)(j) = scan.nextInt()
      }
    }

    val pNum = 4

    var maxProduct = Long.MinValue
    for (i <- 0 until 20) {
      for (j <- 0 until 20) {
        if (j + pNum - 1 < 20) {
          val p = arr(i)(j) * arr(i)(j + 1) * arr(i)(j + 2) * arr(i)(j + 3)
          if (maxProduct < p) maxProduct = p
        }
        if (i + pNum - 1 < 20) {
          val p = arr(i)(j) * arr(i + 1)(j) * arr(i + 2)(j) * arr(i + 3)(j)
          if (maxProduct < p) maxProduct = p
        }
        if (j + pNum - 1 < 20 && i + pNum - 1 < 20) {
          val p = arr(i)(j) * arr(i + 1)(j + 1) * arr(i + 2)(j + 2) * arr(i + 3)(j + 3)
          if (maxProduct < p) maxProduct = p
        }
        if (j - pNum + 1  >= 0 && i + pNum - 1 < 20) {
          val p = arr(i)(j) * arr(i + 1)(j - 1) * arr(i + 2)(j - 2) * arr(i + 3)(j - 3)
          if (maxProduct < p) maxProduct = p
        }
      }
    }
    println(maxProduct)
  }
}
