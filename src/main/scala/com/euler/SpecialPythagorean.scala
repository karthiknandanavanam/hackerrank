package com.euler

object SpecialPythagorean {

  def findMaxTriplet(n: Int) = {
    var triplets = List[Long]()
    for (a <- 1 until n / 3) {
      val b = n * (n/2 - a) / (n - a)
      val c = n - a - b
      if (a * a + b * b == c * c) {
        triplets = triplets ++ List(1L * a * b * c)
      }
    }

    triplets match {
      case List() => -1L
      case a => a.max
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    for (i <- 0 until size) {
      val sum = arr(i)
      val max = findMaxTriplet(sum)
      println(max)
    }
  }
}
