package com.euler

object Multiples3And5 {

  def sumOf3And5(num: Long) = {
    val threeLimit = num / 3
    val fiveLimit = num / 5
    val fifteenLimit = num / 15

    val threeSum = sumSeq(threeLimit) * 3
    val fiveSum = sumSeq(fiveLimit) * 5
    val fifteenSum = sumSeq(fifteenLimit) * 15

    threeSum + fiveSum - fifteenSum
  }

  def sumSeq(n: Long) = n * (n + 1) / 2

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val testCount = scan.next().toInt

    val arr = new Array[Long](testCount)
    for (i <- 0 until testCount) {
      arr(i) = scan.next().toInt
    }

    val resultList = new Array[Long](testCount)
    for (i <-  0 until testCount) {
      val result = sumOf3And5(arr(i) - 1)
      resultList(i) = result
    }

    println(resultList.mkString("\n"))
  }
}
