package com.euler

object LongestCollatzSequence {

  def collatz(a: Long, ac: Long, c: Long, col: Array[Long]): Long = {
    a match {
      case i if i < ac && i <= 5000000 => c + col(i.toInt) - 1L
      case 0 => 0
      case 1 => c
      case i if i % 2 == 0 => collatz(i / 2, ac, c + 1, col)
      case i if i % 2 != 0 => collatz(3 * i + 1, ac, c + 1, col)
    }
  }

  def maxCols(arr: Array[Long]) = {
    val len = arr.length
    val max = new Array[Long](len)
    var maxIndex = (0, 0L)
    var i = 0
    while (i < len) {
      if (arr(i) >= maxIndex._2) {
        maxIndex = (i, arr(i))
      }
      max(i) = maxIndex._1
      i = i + 1
    }
    max
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = (0 until size).map(_ => scan.nextInt())

    val col = new Array[Long](5000001)
    (0 to col.length - 1).foreach(r => col(r) = collatz(r, r, 1, col))
    val maxs = maxCols(col)

    val res = arr.map(r => maxs(r))
    println(res.mkString("\n"))
  }
}
