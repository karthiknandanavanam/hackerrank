package com.euler

object SmallestMultiple {
  def getPrime(value: Int): Int = {
    var primes = List[Int]()
    for (i <- 2 to Math.sqrt(value).toInt) {
      if (!primes.exists(a => i % a == 0)) {
        primes = i :: primes
      }
      if (value % i == 0) {
        return i
      }
    }
    return value
  }

  def factors(n: Int) = {
    var num = n
    var factors = List[Int]()
    while (num != 1) {
      val prime = getPrime(num)
      factors = prime :: factors
      num = num / prime
    }
    factors.map(a => (a, 1))
      .groupBy(a => a._1)
      .map(a => (a._1, a._2.map(b => b._2).sum))
  }

  def lcm(n: Int) = {
    var uniquePrimes = Map[Int, Int]()
    for (i <- 2 to n) {
      val primeFactors = factors(i)
      for (prime <- primeFactors) {
        if (uniquePrimes.getOrElse(prime._1, 0) <= prime._2) {
          uniquePrimes = uniquePrimes + prime
        }
      }
    }

    var result = 1
    for (prime <- uniquePrimes) {
      result = result * Math.pow(prime._1, prime._2).toInt
    }
    result
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    for (i <- 0 until size) {
      val num = arr(i)
      val res = lcm(num)
      println(res)
    }
  }
}
