package com.euler

object SummationOfPrimes {

  def removeNonPrimes(max: Int, seive: Array[Int], i: Int) = {
    var iter = i + seive(i)
    while (iter <= max) {
      seive(iter) = 0
      iter = iter + seive(i)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val max = 1000000
    val seive = (0 to max).toArray
    for (i <- 0 to max) {
      seive(i) match {
        case 0 => seive(i) = 0
        case 1 => seive(i) = 0
        case _ => removeNonPrimes(max, seive, i)
      }
    }

    val sumSeive = seive.map(rec => rec.toLong)
    for (i <- 1 to max) {
      sumSeive(i) = sumSeive(i - 1) + sumSeive(i)
    }

    for (i <- 0 until size) {
      val num = arr(i)
      println(sumSeive(num))
    }
  }
}
