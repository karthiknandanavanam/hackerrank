package com.euler

object LargeSum {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt
    val res = (0 until size).map(_ => scan.nextLine())
      .map(r => BigInt(r))
      .sum
      .toString
    println(res.take(10))
  }
}
