package com.euler

object HighlyDivisibleTriangularNumber {

  def seivePrimes(n: Int) = {
    val primes = (0 to n).toArray
    for (i <- 0 to n) {
      i match {
        case 0 => primes(0) = 0
        case 1 => primes(1) = 0
        case a => {
          var iter = a + a
          while (iter <= n) {
            primes(iter) = 0
            iter = iter + a
          }
        }
      }
    }
    primes.filter(p => p != 0)
  }

  def factor(n: Long, primes: Array[Int]): Long = {
    for (i <- 0 until primes.length) {
      if (n % primes(i) == 0) {
        return primes(i)
      }
    }
    n
  }

  def factors(n: Long, primes: Array[Int]) = {
    var num = n
    var facs = List[Long]()
    while(num != 1) {
      val f = factor(num, primes)
      facs = f :: facs
      num = num / f
    }
    facs.groupBy(p => p).map(rec => rec._2.length + 1).product
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val primes = seivePrimes(10000000)

    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var map = Map[Int, Long]()
    var facsN = arr.sorted
    var iter = 1L
    while (facsN.nonEmpty) {
      val num = iter * (iter + 1) / 2
      val facs = factors(num, primes)
      while (facsN.nonEmpty && facs > facsN.head) {
        map = map ++ Map(facsN.head -> num)
        facsN = facsN.tail
      }
      iter = iter + 1
    }

    for (i <- 0 until size) {
      println(map(arr(i)))
    }
  }
}
