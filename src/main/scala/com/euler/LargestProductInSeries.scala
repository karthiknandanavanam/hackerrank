package com.euler

object LargestProductInSeries {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val num = scan.nextInt()

    val arr = new Array[(Int, Int, Array[Int])](num)
    for (i <- 0 until num) {
      val n = scan.nextInt()
      val k = scan.nextInt()
      val number = scan.next().toCharArray.toSeq.map(rec => Integer.parseInt(rec.toString)).toArray
      arr(i) = (k, n, number)
    }

    for (i <- 0 until num) {
      val k = arr(i)._1
      val n = arr(i)._2
      val array = arr(i)._3
      var max = Int.MinValue

      for (j <- 0 until n - k) {
        var mul = 1
        for (p <- 0 until k) {
          mul = mul * array(j + p)
        }
        if (mul > max) {
          max = mul
        }
      }
      println(max)
    }
  }
}
