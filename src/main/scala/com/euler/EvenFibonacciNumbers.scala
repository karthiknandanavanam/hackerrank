package com.euler

object EvenFibonacciNumbers {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val inputCount = scan.next().toInt

    val arr = new Array[Long](inputCount)
    for (i <- 0 until inputCount) {
      arr(i) = scan.next().toLong
    }

    for (i <- 0 until inputCount) {
      val limit = arr(i)
      var first = 1L
      var second = 2L

      var evenSum = 0L
      while (second <= limit) {
        if (second % 2 == 0) {
          evenSum = evenSum + second
        }
        val temp = first + second
        first = second
        second = temp
      }
      println(evenSum)
    }
  }
}
