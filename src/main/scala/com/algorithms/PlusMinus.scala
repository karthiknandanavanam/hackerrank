package com.algorithms

object PlusMinus {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.next().toInt
    }

    var (zero, pos, neg) = (0, 0, 0)
    for (i <- 0 until size) {
      if (arr(i) > 0) {
        pos += 1
      }
      else if (arr(i) == 0) {
        zero += 1
      }
      else {
        neg += 1
      }
    }

    println("%.6f".format(1.0 * pos / size))
    println("%.6f".format(1.0 * neg / size))
    println("%.6f".format(1.0 * zero / size))
  }
}
