package com.algorithms

object CamelCase {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val input = scan.next()

    input match {
      case a if a.isEmpty => println("0")
      case a => {
        var count = 1
        for (char <- input) {
          char match {
            case b if 65 <= b && b <= 90 => count = count + 1
            case b => count = count
          }
        }
        println(count)
      }
    }
  }
}
