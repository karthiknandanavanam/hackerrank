package com.algorithms

object Pangrams {

  def isPangram(str: String): Boolean = {
    val bits = new java.util.BitSet()
    bits.clear()
    str.foreach { rec =>
      rec match {
        case a if 65 <= a && a <= 90 => if (!bits.get((a - 65) % 26)) bits.flip((a - 65) % 26)
        case a if 97 <= a && a <= 122 => if (!bits.get((a - 97) % 26)) bits.flip((a - 97) % 26)
        case _ =>
      }
    }

    for (i <- 0 until 26) {
      if (!bits.get(i)) {
        return false
      }
    }
    true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()

    input match {
      case a if isPangram(a) => println("pangram")
      case a => println("not pangram")
    }
  }
}
