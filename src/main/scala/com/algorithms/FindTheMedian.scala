package com.algorithms

object FindTheMedian {

  def findMedian(arr: Array[Int], size: Int): Int = {
    val mid = (size - 1) / 2
    var done = false
    var li = arr
    while (!done) {
      val m = li(mid)
      val (lLeft, lRight) = (0 until mid).map(r => li(r)).partition(n => n <= m)
      val (rLeft, rRight) = ((mid + 1) until size).map(r => li(r)).partition(n => n <= m)

      li = (lLeft ++ rLeft ++ Array(m) ++ lRight ++ rRight).toArray
      if (li(mid) == m) {
        done = true
        return li(mid)
      }
    }
    throw new IllegalArgumentException()
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = (0 until size).map(_ => scan.nextInt()).toArray
    println(findMedian(arr, size))
  }
}
