package com.algorithms

object TimeConversion {

  def timeConversion(time: String) = {
    val hour = time.substring(0, 2).toInt
    val amOrPm = time.substring(8, 10)

    (amOrPm, hour) match  {
      case ("AM", 12) => "00" + time.substring(2, 8)
      case ("AM", _) => time.substring(0, 8)
      case ("PM", 12) => time.substring(0, 8)
      case ("PM", _) => {
        val newFormat = hour + 12
        newFormat + time.substring(2, 8)
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val twelveHourFormat = scan.next()
    val twentyFourFormat = timeConversion(twelveHourFormat)
    println(twentyFourFormat)
  }
}
