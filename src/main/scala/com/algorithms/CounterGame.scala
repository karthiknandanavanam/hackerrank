package com.algorithms

object CounterGame {

  def findWinner(a: Long) = {
    var num = a
    var winner = "Louise"
    while (num > 1) {
      if (isPowerOf2(num)) {
        num = num / 2
        winner = swap(winner)
      }
      else {
        num = num - leastPowerOf2(num)
        winner = swap(winner)
      }
    }
    swap(winner)
  }

  def swap(a: String) = {
    a match {
      case "Louise" => "Richard"
      case "Richard" => "Louise"
    }
  }

  def leastPowerOf2(a: Long) = {
    var pow = 1L
    while(a > pow) {
      pow = pow * 2
    }
    pow / 2
  }

  def isPowerOf2(a: Long): Boolean = {
    var num = a
    while (num >= 2) {
      if (num % 2 != 0) {
        return false
      }
      else {
        num = num / 2
      }
    }
    true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[Long](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextLong()
    }

    for (i <- 0 until size) {
      val num = arr(i)
      val winner = findWinner(num)
      println(winner)
    }
  }
}
