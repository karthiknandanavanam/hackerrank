package com.algorithms

object BreakingTheRecords {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var max = arr(0)
    var maxRecs = 0
    var min = arr(0)
    var minRecs = 0

    for (i <- 0 until size) {
      val score = arr(i)
      if (score < min) {
        min = score
        minRecs = minRecs + 1
      }
      if (score > max) {
        max = score
        maxRecs = maxRecs + 1
      }
    }

    println(maxRecs + " " + minRecs)
  }
}
