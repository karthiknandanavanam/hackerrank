package com.algorithms

object GridChallenge {

  def sortRow(mat: Array[Array[Char]], matSize: Int) = {
    (0 until matSize) foreach { i =>
      (0 until matSize) foreach { k =>
        (1 until matSize) foreach { j =>
          if (mat(i)(j) < mat(i)(j - 1)) {
            val temp = mat(i)(j)
            mat(i)(j) = mat(i)(j - 1)
            mat(i)(j - 1) = temp
          }
        }
      }
    }
  }

  def isSortColumn(mat: Array[Array[Char]], matSize: Int): Boolean = {
    (0 until matSize) foreach { j =>
      (0 until matSize) foreach { k =>
        (1 until matSize) foreach { i =>
          if (mat(i)(j) < mat(i - 1)(j)) {
            return false
          }
        }
      }
    }
    true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    (0 until size).foreach(_ => {
      val matSize = scan.nextInt()
      val mat = Array.ofDim[Char](matSize, matSize)
      (0 until matSize) foreach { i =>
        val chars = scan.next()
        (0 until matSize) foreach { j =>
          mat(i)(j) = chars(j)
        }
      }

      sortRow(mat, matSize)
      val res = isSortColumn(mat, matSize) match {
        case true => "YES"
        case false => "NO"
      }
      println(res)
    })
  }
}
