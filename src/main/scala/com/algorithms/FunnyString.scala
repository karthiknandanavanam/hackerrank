package com.algorithms

object FunnyString {

  def funny(str: String):String = {
    val len = str.length
    var l = 1
    var r = len - 2
    while (l <= len - 1 && r >= 0) {
      val lv = Math.abs(str(l) - str(l - 1))
      val rv = Math.abs(str(r) - str(r + 1))
      if (lv != rv) {
        return "Not Funny"
      }
      l = l + 1
      r = r - 1
    }
    "Funny"
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = (0 until size).map(_ => scan.next())
    val res = arr.map(r => funny(r))
    println(res.mkString("\n"))
  }
}
