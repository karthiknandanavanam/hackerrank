package com.algorithms

object SeparateTheNumbers {

  def beautiful(n: String): String = {

    def recur(s: String, n: List[BigInt]): List[String] = {
      val ne = n.head + 1
      val neLen = ne.toString.length
      s match {
        case a if a.length == 0 && n.length > 1 => List(s"YES ${n.last}")
        case a if a.length == 0 && n.length <= 1 => List(s"NO")
        case a if a.length >= neLen && a.substring(0, neLen).toLong == ne => recur(a.substring(neLen, a.length), BigInt(a.substring(0, neLen)) :: n)
        case a if a.length >= neLen && a.substring(0, neLen).toLong != ne => List("NO")
        case a if a.length < neLen => List("NO")
      }
    }

    for (i <- 1 until n.length) {
      val s = List(BigInt(n.substring(0, i)))
      val r = n.substring(i, n.length)
      val res = recur(r, s).filter(p => p != "NO")
      if (res.nonEmpty) return res.head
    }
    "NO"
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt
    val nums = (0 until size).map(_ => scan.next())
    for (i <- nums) {
      val res = beautiful(i)
      println(res)
    }
  }
}
