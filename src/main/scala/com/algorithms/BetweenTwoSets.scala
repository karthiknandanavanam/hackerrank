package com.algorithms

object BetweenTwoSets {

  def isDivisible(i: Int, ints: Array[Int]): Boolean = {
    ints.count(rec => i % rec == 0) == ints.length
  }

  def isDivided(ints: Array[Int], i: Int): Boolean = {
    ints.count(rec => rec % i == 0) == ints.length
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val sizeA = scan.nextInt()
    val sizeB = scan.nextInt()

    val arrA = new Array[Int](sizeA)
    for (i <- 0 until sizeA) {
      arrA(i) = scan.nextInt()
    }

    val arrB = new Array[Int](sizeB)
    for (i <- 0 until sizeB) {
      arrB(i) = scan.nextInt()
    }

    val start = arrA.max
    val end = arrB.min
    var count = 0
    for (i <- start to end) {
      if (isDivisible(i, arrA) && isDivided(arrB, i)) {
        count = count + 1
      }
    }
    println(count)
  }
}
