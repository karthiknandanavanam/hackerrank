package com.algorithms

object BigSorting {

  def compare(a: String, b: String) = {
    if (a.length < b.length) {
      true
    }
    else if (a.length == b.length) {
      a.compareTo(b) < 0
    }
    else {
      false
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[String](size)
    for (i <- 0 until size) {
      arr(i) = scan.next()
    }

    val res = arr.sortWith((a, b) => compare(a, b))
    println(res.mkString("\n"))
  }
}
