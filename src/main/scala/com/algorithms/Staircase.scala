package com.algorithms

object Staircase {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    for(i <- 1 to size) {
      for (j <- size - i to 1 by -1) {
        print(" ")
      }
      for (j <- 1 to i) {
        print("#")
      }
      println()
    }
  }
}
