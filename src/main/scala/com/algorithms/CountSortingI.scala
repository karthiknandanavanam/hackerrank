package com.algorithms

object CountSortingI {

  def count(arr: Array[Int]) = {
    val out = new Array[Int](100)
    arr.foreach(i => out(i) = out(i) + 1)
    out
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val counter = count(arr)
    println(counter.mkString(" "))
  }
}
