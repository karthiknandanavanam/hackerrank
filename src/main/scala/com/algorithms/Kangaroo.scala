package com.algorithms

object Kangaroo {
  def findMatch(aS: Int, aJ: Int, bS: Int, bJ: Int) = {
    var aJump = aS + aJ
    var bJump = bS + bJ
    while (aJump < bJump) {
      aJump = aJump + aJ
      bJump = bJump + bJ
    }
    if (aJump == bJump) {
      println("YES")
    }
    else {
      println("NO")
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val aS = scan.nextInt()
    val aJ = scan.nextInt()
    val bS = scan.nextInt()
    val bJ = scan.nextInt()

    if (aS < bS && aJ > bJ) {
      findMatch(aS, aJ, bS, bJ)
    }
    else if (aS > bS && aJ < bJ) {
      findMatch(bS, bJ, aS, aJ)
    }
    else {
      println("NO")
    }
  }
}
