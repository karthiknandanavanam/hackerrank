package com.algorithms

object IntroToTutorialChallenges {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val look = scan.nextInt()
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var i = 0
    while (i < size && arr(i) != look) {
      i = i + 1
    }

    if (i != size) {
      println(i)
    }
  }
}
