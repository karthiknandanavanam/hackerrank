package com.algorithms

object AppleAndOrange {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val homeS = scan.nextInt()
    val homeT = scan.nextInt()

    val appleA = scan.nextInt()
    val orangeB = scan.nextInt()

    val appleSize = scan.nextInt()
    val orangeSize = scan.nextInt()

    var applesInHouse = 0
    for (i <- 0 until appleSize) {
      val dFromTree = scan.nextInt()
      val pos = appleA + dFromTree
      if (homeS <= pos && pos <= homeT) {
        applesInHouse += 1
      }
    }

    println(applesInHouse)

    var orangesInHouse = 0
    for (i <- 0 until orangeSize) {
      val dFromTree = scan.nextInt()
      val pos = orangeB + dFromTree
      if (homeS <= pos && pos <= homeT) {
        orangesInHouse += 1
      }
    }

    println(orangesInHouse)
  }
}
