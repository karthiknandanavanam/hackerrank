package com.algorithms


object TheFullCountingSort {

  def sort(scan: java.util.Scanner, size: Int) = {
    val rank = new Array[scala.collection.mutable.Queue[String]](100).map(rec => scala.collection.mutable.Queue[String]())
    val halfSize = size / 2

    for (i <- 0 until size) {
      val (count, string) = (scan.nextInt(), scan.next())
      if (i < halfSize) {
        rank(count).enqueue("-")
      }
      else {
        rank(count).enqueue(string)
      }
    }

    val ar = scala.collection.mutable.Queue[String]()
    for (i <- 0 until 100) {
      val que = rank(i)
      while (que.nonEmpty) {
        ar.enqueue(que.dequeue())
      }
    }
    ar
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val res = sort(scan, size)
    println(res.mkString(" "))
  }
}
