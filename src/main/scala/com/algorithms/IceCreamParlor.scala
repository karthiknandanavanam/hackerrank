package com.algorithms

object IceCreamParlor {

  def flavors(arr: Array[Int], money: Int): (Int, Int) = {
    arr.indices.foreach { i =>
      val left = money - arr(i)
      if (left <= 0) {
      }
      else {
        for (j <- i + 1 until arr.length) {
          if (arr(j) == left) {
            return (i + 1, j + 1)
          }
        }
      }
    }
    (0, 0)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    (0 until size).foreach {_ =>
      val money = scan.nextInt()
      val flavSize = scan.nextInt()
      val arr = (0 until flavSize).map(_ => scan.nextInt()).toArray
      val res = flavors(arr, money)
      println(s"${res._1} ${res._2}")
    }
  }
}
