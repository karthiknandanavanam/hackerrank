package com.algorithms

object AlternatingCharacters {

  def alternate(in: List[Char], s: Char): Int = {
    (in, s) match {
      case (_, b) if b == '0' => 0 + alternate(in.tail, in.head)
      case (a, _) if a.isEmpty => 0
      case (a, b) if b == a.head => 1 + alternate(a.tail, s)
      case (a, b) if b != a.head => 0 + alternate(a.tail, a.head)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val inputs = (0 until size).map(_ => scan.next())

    val res = inputs.map(r => alternate(r.toList, '0'))
    println(res.mkString("\n"))
  }
}
