package com.algorithms

object MiniMaxSum {

  def minimum(arr: Array[Long]) = {
    var min = arr(0)
    for (i <- 1 until arr.length) {
      if (min > arr(i)) {
        min = arr(i)
      }
    }
    min
  }

  def maximum(arr: Array[Long]) = {
    var max = arr(0)
    for (i <- 1 until arr.length) {
      if (max < arr(i)) {
        max = arr(i)
      }
    }
    max
  }

  def findSum(arr: Array[Long]) = {
    var sum = 0L
    for (i <- 0 until arr.length) {
      sum = sum + arr(i)
    }
    sum
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    var arr = new Array[Long](5)
    for (i <- 0 until 5) {
      arr(i) = scan.nextLong()
    }

    val min = minimum(arr)
    val max = maximum(arr)
    val sum = findSum(arr)

    val minSum = sum - max
    val maxSum = sum - min
    println(minSum + " " + maxSum)
  }
}
