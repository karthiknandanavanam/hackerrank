package com.algorithms

object BirthdayChocolate {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val d = scan.nextInt()
    val m = scan.nextInt()

    val sum = new Array[Int](size + 1)
    sum(0) = 0
    for (i <- 1 to size) {
      sum(i) = arr(i - 1)
      sum(i) = sum(i) + sum(i - 1)
    }

    def patternMatch = {
      var count = 0
      for (i <- 1 to size) {
        val rStart = i - 1
        val rEnd = i + m - 1

        if (rEnd <= size && rStart >= 0) {
          val s = sum(rEnd) - sum(rStart)
          if (s == d) {
            count = count + 1
          }
        }
      }
      count
    }

    val count: Int = patternMatch
    println(count)
  }
}
