package com.algorithms

object BirthdayCakeCandles {

  def birthdayCakeCandles(n: Int, arr: Array[Int]): Int = {
    var count = 0
    var max = 0
    for (i <- 0 until n) {
      if (max == arr(i)) {
        count = count + 1
      }
      else if (max < arr(i)) {
        max = arr(i)
        count = 1
      }
    }
    count
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val count = birthdayCakeCandles(size, arr)
    println(count)
  }
}
