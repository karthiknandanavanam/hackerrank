package com.algorithms

object ThePowerSum {

  def main(args: Array[String]) = {
    val xN = scala.io.Source.stdin.getLines().take(2).map(r => r.toInt).toArray
    val x = xN(0)
    val n = xN(1)

    val nums = (1 to x).map(r => Math.pow(r, n).toInt).takeWhile(r => r <= x).toArray

    def comb(nums: Array[Int], sum: Int): Int = {
      (nums, sum) match {
        case (_, s) if s == x => 1
        case (n, s) if n.isEmpty && s == x => 1
        case (n, s) if n.isEmpty && s != x => 0
        case (n, s) if n.nonEmpty && s < x => comb(n.tail, s + n.head) + comb(n.tail, s)
        case (n, s) if n.nonEmpty && s > x => 0
      }
    }

    val res = comb(nums, 0)
    println(res)
  }
}
