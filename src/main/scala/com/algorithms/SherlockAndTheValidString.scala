package com.algorithms

object SherlockAndTheValidString {

  def isValid(in: String) = {
    val res = in.map(r => r)
      .groupBy(r => r)
      .map(r => (r._1, r._2.length))
      .groupBy(r => r._2)
      .map(r => (r._1, r._2.keys.toList.length))
      .toList
      .sorted
      .toArray

    res match {
      case a if a.length == 1 => true
      case a if a.length > 2 => false
      case a if a.length == 2 && a(0)._1 == 1 && a(0)._2 == 1 => true
      case a if a.length == 2 && a(1)._2 == 1 && a(1)._1 == a(0)._1 + 1 => true
      case _ => false
    }
  }

  def main(args: Array[String]) = {
    val in = scala.io.Source.stdin.getLines().next()
    val res = isValid(in)

    res match {
      case true => println("YES")
      case false => println("NO")
    }
  }
}
