package com.algorithms

object MakingAnagrams {

  def anagram(a: String, b: String) = {
    val aArr = new Array[Int](26).map(_ => 0)
    val bArr = new Array[Int](26).map(_ => 0)
    a.map(r => r - 97).foreach(i => aArr(i) = aArr(i) + 1)
    b.map(r => r - 97).foreach(i => bArr(i) = bArr(i) + 1)
    val common = (0 until 26).map(i => Math.min(aArr(i), bArr(i))).sum
    (a.length - common) + (b.length - common)
  }

  def main(args: Array[String]) = {
    val inputs = scala.io.Source.stdin.getLines().take(2)
    val a = inputs.next()
    val b = inputs.next()
    val c = anagram(a, b)
    println(c)
  }
}
