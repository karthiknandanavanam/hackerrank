package com.algorithms

object SimpleArraySum {
  def recursiveSum(ar: Array[Int]) = {
    def addSum(sum: Int, index: Int): Int = {
      if (index < ar.length) {
        addSum(sum + ar(index), index + 1)
      }
      else {
        sum
      }
    }

    addSum(0, 0)
  }

  def simpleArraySum(n: Int, ar: Array[Int]): Int =  {
    var sum = 0
    for (i <- 0 until n) {
      sum = sum + ar(i)
    }
    sum
  }

  def main(args: Array[String]) = {
    val scanner = new java.util.Scanner(System.in)
    val n = scanner.next().toInt

    val array = new Array[Int](n)
    for (i <- 0 until n) {
      array(i) = scanner.next().toInt
    }
    val sum = recursiveSum(array)
    println(sum)
  }
}
