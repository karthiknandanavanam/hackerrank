package com.algorithms

object GradingStudents {

  def round(n: Int): Int = {
    val diffToNextMultiple = 5 - n % 5
    (n, diffToNextMultiple) match {
      case (a, b) if a < 38 => a
      case a if a._2 < 3 => n + a._2
      case _ => n
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    for (i <- 0 until size) {
      println(round(arr(i)))
    }
  }
}
