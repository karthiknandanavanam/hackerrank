package com.algorithms

object BeautifulBinaryString {
  val w = Array(0, 1, 0)

  def beautifulBinaryString(arr: Array[Int], size: Int, i: Int): Int = {
    arr match {
      case a if size - i < 3 => 0
      case a if a(i) == 1 => beautifulBinaryString(arr, size, i + 1)
      case a if w sameElements Array(a(i), a(i + 1), a(i + 2)) => a(i + 2) = 1; 1 + beautifulBinaryString(arr, size, i + 2)
      case _ => beautifulBinaryString(arr, size, i + 1)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val bin = scan.next().map(r => r.asDigit).toArray
    val res = beautifulBinaryString(bin, size, 0)
    println(res)
  }
}
