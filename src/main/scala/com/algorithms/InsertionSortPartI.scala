package com.algorithms

object InsertionSortPartI {

  def insertPos(arr: Array[Int], pos: Int): Unit = {
    val num = arr(pos)
    var i = pos - 1
    while (i >= 0) {
      if (arr(i) >= num) {
        arr(i + 1) = arr(i)
      }
      else {
        arr(i + 1) = num
        println(arr.mkString(" "))
        return
      }
      println(arr.mkString(" "))
      i = i - 1
    }
    if (i == -1) {
      arr(0) = num
      println(arr.mkString(" "))
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val pos = size - 1
    insertPos(arr, pos)
  }
}
