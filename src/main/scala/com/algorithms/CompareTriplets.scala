package com.algorithms

object CompareTriplets {

  def compareInts(a: Int, b: Int) = {
    val compare = a.compareTo(b)
    compare match {
      case -1 => 0
      case 1 => 1
      case 0 => 0
    }
  }

  def compare(aVec: (Int, Int, Int), bVec: (Int, Int, Int)) = {
    compareInts(aVec._1, bVec._1) + compareInts(aVec._2, bVec._2) + compareInts(aVec._3, bVec._3)
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val arr = new Array[Int](6)
    for (i <- 0 until 6) {
      arr(i) = scan.next().toInt
    }

    val aVec = (arr(0), arr(1), arr(2))
    val bVec = (arr(3), arr(4), arr(5))
    val compareAB = compare(aVec, bVec)
    val compareBA = compare(bVec, aVec)

    println(compareAB + " " + compareBA)
  }
}
