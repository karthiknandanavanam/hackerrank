package com.algorithms

object QuickSortIPartition {

  def partition(arr: Array[Int]) = {
    if (arr.isEmpty || arr.length == 1) {
      arr
    }
    else {
      val p = arr(0)
      var left = new Array[Int](0)
      var right = new Array[Int](0)
      for (i <- 1 until arr.length) {
        if (arr(i) <= p) {
          left = left ++ List(arr(i))
        }
        else {
          right = right ++ List(arr(i))
        }
      }
      left ++ List(p) ++ right
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val res = partition(arr)
    println(res.mkString(" "))
  }
}
