package com.algorithms

object WeightedUniformStrings {

  def uniformWeights(arr: Array[Int], i: Int) = {
    val num = arr(i)
    var iter = i + 1
    var p = arr(i)
    var weightsP = List[Int](p)
    while (iter < arr.length && arr(iter) == num) {
      iter = iter + 1
      p = p + arr(i)
      weightsP = p :: weightsP
    }

    weightsP
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val s = scan.nextLine()

    val size = scan.nextLine().toInt
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextLine().toInt
    }

    val weights = s.map(rec => rec - 97).map(rec => rec + 1).toArray
    var w = Set[Int]()
    var i = 0
    while (i < weights.length) {
      val r = uniformWeights(weights, i)
      i = i + r.length
      w = w ++ r
    }

    for (i <- arr.indices) {
      if (w.contains(arr(i))) {
        println("Yes")
      }
      else {
        println("No")
      }
    }
  }

}
