package com.algorithms

object MaximizingXOR {

  def maxXor(s: Int, e: Int) = {
    var max = Int.MinValue
    for (i <- s to e) {
      for (j <- i to e) {
        val xor = i ^ j
        if (max < xor) {
          max = xor
        }
      }
    }
    max
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val s = scan.nextInt()
    val e = scan.nextInt()

    val max = maxXor(s, e)
    println(max)
  }
}
