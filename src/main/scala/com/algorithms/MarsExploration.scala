package com.algorithms

object MarsExploration {

  def changeInSOS(a: String, ind: Int) = {
    val sos = "SOS"
    var count = 0
    for (i <- 0 until sos.size) {
      if (sos(i) != a(ind + i)) {
        count = count + 1
      }
    }
    count
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val input = scan.next()
    input match {
      case a if a.size % 3 == 0 => {
        var count = 0
        for (i <- 0 until a.size by 3) {
          val c = changeInSOS(a, i)
          count = count + c
        }
        println(count)
      }
    }
  }
}
