package com.algorithms

object ClosestNumbers {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val sorted = arr.sorted
    var minDiff = Int.MaxValue
    for (i <- 1 until size) {
      val diff = sorted(i) - sorted(i - 1)
      if (minDiff > diff) {
        minDiff = diff
      }
    }

    for (i <- 1 until size) {
      val diff = sorted(i) - sorted(i - 1)
      if (diff == minDiff) {
        print(sorted(i - 1) + " " + sorted(i) + " ")
      }
    }
  }
}
