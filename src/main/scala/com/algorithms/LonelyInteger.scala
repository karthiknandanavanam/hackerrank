package com.algorithms

object LonelyInteger {

  def lonelyInteger(arr: Array[Int]) = {
    var res = 0
    for (i <- 0 until arr.length) {
      res = res ^ arr(i)
    }
    res
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val num = scan.nextInt()
    val arr = new Array[Int](num)
    for (i <- 0 until num) {
      arr(i) = scan.nextInt()
    }

    val res = lonelyInteger(arr)
    println(res)
  }
}
