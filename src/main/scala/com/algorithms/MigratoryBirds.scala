package com.algorithms

object MigratoryBirds {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](5)
    for (i <- 0 until size) {
      val num = scan.nextInt()
      arr(num - 1) = arr(num - 1) + 1
    }

    val res = arr.zipWithIndex.maxBy(a => a._1)
    println(res._2 + 1)
  }
}
