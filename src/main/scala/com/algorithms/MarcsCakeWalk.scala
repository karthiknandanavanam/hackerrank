package com.algorithms

object MarcsCakeWalk {

  def marcsCakeWalk(calories: Array[Int]) = {
    val s = calories.sortWith((a, b) => a >= b)
    val miles = s.zipWithIndex.map(c => c._1 * Math.pow(2, c._2).toLong).sum
    miles
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val calories = (0 until size).map(_ => scan.nextInt()).toArray
    val miles = marcsCakeWalk(calories)
    println(miles)
  }
}
