package com.algorithms

object DiagonalDifference {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt
    val arr = Array.ofDim[Int](size, size)
    for (i <- 0 until size) {
      for (j <- 0 until size) {
        arr(i)(j) = scan.next().toInt
      }
    }

    var leftSum = 0
    var rightSum = 0
    for (i <- 0 until size) {
      leftSum += arr(i)(i)
      rightSum += arr(i)(size - 1 - i)
    }
    println(Math.abs(leftSum - rightSum).toInt)
  }
}
