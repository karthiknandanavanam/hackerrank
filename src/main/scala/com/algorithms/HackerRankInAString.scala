package com.algorithms

object HackerRankInAString {

  def hasHackerRank(str: String) = {
    var hackerRank = "hackerrank"
    var input = str
    while (!input.isEmpty && !hackerRank.isEmpty) {
      if (hackerRank.head == input.head) {
        hackerRank = hackerRank.tail
      }
      input = input.tail
    }
    hackerRank match {
      case a if a.isEmpty => true
      case a => false
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[String](size)
    for (i <- 0 until size) {
      arr(i) = scan.next()
    }

    for (i <- 0 until size) {
      val str = arr(i)
      str match {
        case a if hasHackerRank(a) => println("YES")
        case a => println("NO")
      }
    }
  }
}
