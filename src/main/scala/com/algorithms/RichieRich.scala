package com.algorithms

/*
8 4
11119111
 */
object RichieRich {

  def convert(in: Array[Char], n: Int, k: Int) = {
    val misPairs = mismatchedPairs(in, n)
    (n, k, misPairs) match {
      case (_, b, _) if b < misPairs => Array('-', '1')
      case (_, b, _) if b == misPairs => matchExact(in, n, k, misPairs)
      case (_, b, _) if b > misPairs => matchExcess(in, n, k, misPairs)
    }
  }

  def matchExcess(in: Array[Char], n: Int, k: Int, misPairs: Int) = {

    var mPairs = misPairs
    var change = k
    var i = 0
    var j = n - 1
    while (i <= j) {
      val isMispair = in(i) != in(j)
      val isSame = i == j
      val isOneMax = (in(i) == '9' && in(j) != '9') || (in(i) != '9' && in(j) == '9')
      val isMaxAlready = in(i) == '9' && in(j) == '9'

      (isSame, isMispair, mPairs, change, isOneMax, isMaxAlready) match {
        case (_, _, _, _, _, true) =>
        case (true, _, _, c, _, _) if c > 0 => in(i) = '9'; change = change - 1
        case (true, _, _, c, _, _) if c <= 0 =>
        case (_, false, 0, 1, _, _) =>
        case (_, false, 0, 0, _, _) =>
        case (_, false, 0, c, false, false) if c >= 2 => change = change - 2; in(i) = '9'; in(j) = '9'

        case (false, true, _, _, true, _) => mPairs = mPairs - 1; change = change - 1; in(i) = '9'; in(j) = '9'
        case (false, true, m, c, false, _) if m < c && Math.abs(m - c) > 1 => mPairs = mPairs - 1; change = change - 2; in(i) = '9'; in(j) = '9'
        case (false, true, m, c, false, _) if m == c => change = change - 1; mPairs = mPairs - 1; val res = if (in(i) < in(j)) in(j) else in(i); in(i) = res; in(j) = res
        case (false, true, m, c, false, _) if m < c && Math.abs(m - c) == 1 => mPairs = mPairs - 1; change = change - 2; in(i) = '9'; in(j) = '9'


        case (false, false, m, c, true, _) if m < c && Math.abs(m - c) > 1 => change = change - 1; in(i) = '9'; in(j) = '9'
        case (false, false, m, c, false, false) if m < c && Math.abs(m - c) >= 2 => change = change - 2; in(i) = '9'; in(j) = '9'
        case (false, false, _, _, false, _) =>
      }
      i = i + 1
      j = j - 1
    }
    in
  }

  def matchExact(in: Array[Char], n: Int, k: Int, misPairs: Int) = {
    var i = 0
    var j = n - 1
    while (i < j) {
      if (in(i) != in(j)) {
        val res = if (in(i) < in(j)) in(j) else in(i)
        in(i) = res
        in(j) = res
      }
      i = i + 1
      j = j - 1
    }
    in
  }

  def mismatchedPairs(in: Array[Char], n: Int) = {
    var i = 0
    var j = n - 1
    var count = 0
    while (i < j) {
      if (in(i) != in(j)) {
        count = count + 1
      }
      i = i + 1
      j = j - 1
    }
    count
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val n = scan.nextInt()
    val k = scan.nextInt()
    val in = scan.next().toCharArray
    val res = convert(in, n, k)
    println(res.mkString(""))
  }
}
