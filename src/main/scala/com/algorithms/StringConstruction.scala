package com.algorithms

object StringConstruction {

  def cost(s: String) = {
    s.map(r => r).distinct.length
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val inputs = (0 until size).map(_ => scan.next())
    val res = inputs.map(r => cost(r))
    println(res.mkString("\n"))
  }
}
