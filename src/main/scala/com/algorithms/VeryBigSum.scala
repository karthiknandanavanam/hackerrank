package com.algorithms

object VeryBigSum {
  def aVeryBigSum(n: Int, arr: Array[Long]) = {
    var bigSum = 0L
    for (i <- 0 until n) {
      bigSum = bigSum + arr(i)
    }
    bigSum
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.next().toInt

    val arr = new Array[Long](size)
    for (i <- 0 until size) {
      val num = scan.next().toLong
      arr(i) = num
    }

    println(aVeryBigSum(size, arr))
  }
}
