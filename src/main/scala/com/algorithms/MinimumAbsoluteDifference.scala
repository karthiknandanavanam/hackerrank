package com.algorithms

object MinimumAbsoluteDifference {

  def minimumAbsoluteDifference(size: Int, input: Array[Int]) = {
    val s = input.sorted
    val output = (1 until size).map(r => s(r) - s(r - 1)).min
    output
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val input = (0 until size).map(_ => scan.nextInt()).toArray
    val output: Int = minimumAbsoluteDifference(size, input)
    println(output)
  }
}
