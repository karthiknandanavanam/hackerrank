package com.algorithms

object RunningTimeOfAlgorithms {

  def insert(arr: Array[Int], pos: Int) = {
    val num = arr(pos)

    var i = pos - 1
    var count = 0
    while (i >= 0 && arr(i) > num) {
      arr(i + 1) = arr(i)
      i = i - 1
      count = count + 1
    }
    if (i + 1 != pos) {
      arr(i + 1) = num
      count
    }
    else {
      0
    }
  }

  def shifts(arr: Array[Int]) = {
    var count = 0
    for (i <- 1 until arr.length) {
      val s = insert(arr, i)
      count = count + s
    }
    count
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    println(shifts(arr))
  }
}
