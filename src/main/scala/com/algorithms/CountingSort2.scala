package com.algorithms

object CountingSort2 {

  def counter(arr: Array[Int]) = {
    val out = new Array[Int](100)
    arr.foreach(i => out(i) = out(i) + 1)
    out
  }

  def sort(arr: Array[Int], size: Int) = {
    val out = new Array[Int](size)
    var iter = 0
    for (i <- 0 until arr.length) {
      val count = arr(i)
      for (j <- 0 until count) {
        out(iter) = i
        iter = iter + 1
      }
    }
    out
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val count = counter(arr)
    val sorted = sort(count, size)
    println(sorted.mkString(" "))
  }
}
