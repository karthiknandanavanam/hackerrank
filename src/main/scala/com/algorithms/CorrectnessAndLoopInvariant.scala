package com.algorithms

object CorrectnessAndLoopInvariant {
  def insert(arr: Array[Int], pos: Int): Unit = {
    val num = arr(pos)
    var i = pos - 1
    while (i >= 0 && arr(i) >= num) {
      arr(i + 1) = arr(i)
      i = i - 1
    }

    arr(i + 1) = num
  }

  def sort(arr: Array[Int]) = {
    if (arr.length <= 1) {
      println(arr.mkString(" "))
    }
    else {
      for (i <- 1 until arr.length) {
        insert(arr, i)
        println(arr.mkString(" "))
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    sort(arr)
  }
}
