package com.algorithms

object Anagram {

  def anagram(str: String) = {
    str match {
      case a if a.length % 2 != 0 => -1
      case a if a.length % 2 == 0 => length(a)
    }
  }

  def length(str: String) = {
    val len = str.length
    val mid = len / 2
    val f = str.substring(0, mid)
    val s = str.substring(mid, len)

    val fs = new Array[Int](26).map(_ => 0)
    val ss = new Array[Int](26).map(_ => 0)
    f.map(r => r.toInt - 97).foreach(r => fs(r) = fs(r) + 1)
    s.map(r => r.toInt - 97).foreach(r => ss(r) = ss(r) + 1)

    val common = (0 until 26).map(r => Math.min(fs(r), ss(r))).sum
    mid - common
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = (0 until size).map(_ => scan.next())

    val res = arr.map(r => anagram(r))
    println(res.mkString("\n"))
  }
}
