package com.algorithms

import scala.collection.mutable

/*
4 3
1 2 3

10 4
2 5 3 6

166 23
5 37 8 39 33 17 22 32 13 7 10 35 40 2 43 49 46 19 41 1 12 11 28
 */
object TheCoinChangeProblem {

  def coinCount(coins: List[Int], sum: Int, used: Int): Long = {
    println(used + " " + sum)
    if (coins.isEmpty) {

    }
    if (sum < 0) {
      0L
    }
    else if (sum == 0) {
      1L
    }
    else {
      val res = coins.filter(r => r >= used)
        .map(r => (r, sum - r))
        .map(r => coinCount(coins, r._2, r._1))
        .sum
      res
    }
  }

  val memo = mutable.Map[Int, Array[Long]]()
  def coinChange(cIndex: Int, sum: Int, coins: Array[Int]) = {
    val arr = new Array[Long](sum)
    (0 until sum).foreach { a =>
      val previous = prev(arr, cIndex, a, coins)
      val top = topper(cIndex, a, coins)
      arr(a) = top + previous
    }
    memo.put(coins(cIndex), arr)
  }

  def topper(i: Int, a: Int, coins: Array[Int]) = {
    val top = i - 1
    if (a == 0) {
      1L
    }
    else if (top < 0) {
      0L
    }
    else {
      memo(coins(top))(a)
    }
  }

  def prev(arr: Array[Long], i: Int, a: Int, coins: Array[Int]) = {
    val prev = a - coins(i)
    if (coins(i) == 1) {
      1L
    }
    else if (prev < 0) {
      0L
    }
    else {
      arr(prev)
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val sum = scan.nextInt()
    val size = scan.nextInt()
    val coins = (0 until size).map(_ => scan.nextInt()).toList.sorted.toArray
    (0 until size).map(r => coinChange(r, sum + 1, coins))
    println(memo(coins.max)(sum))
  }
}
