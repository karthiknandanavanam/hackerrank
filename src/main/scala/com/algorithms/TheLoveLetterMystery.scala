package com.algorithms

object TheLoveLetterMystery {

  def opForPalindrome(arr: Array[Char], i: Int, j: Int): Int = {
    (i, j) match {
      case (a, b) if a < b => Math.abs(arr(i) - arr(j)) + opForPalindrome(arr, i + 1, j - 1)
      case _ => 0
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val inputs = (0 until size).map(_ => scan.next())
    val res = inputs.map(r => opForPalindrome(r.toCharArray, 0, r.length - 1))
    println(res.mkString("\n"))
  }
}
