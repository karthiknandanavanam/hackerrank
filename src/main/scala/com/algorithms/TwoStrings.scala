package com.algorithms

object TwoStrings {

  def includeCombinations(): Set[Set[Char]] = {
    var set = Set[Set[Char]]()
    for (i <- 'a' to 'z') {
      for (j <- 'a' to 'z') {
        if (i != j) {
          set = set ++ Set(Set(i, j))
        }
      }
    }
    set
  }

  def isPattern(value: String): Boolean = {
    value match {
      case value if value.isEmpty => false
      case value if value.size == 1 => false
      case value => {
        val first = value(0)
        val second = value(1)
        if (first == second) {
          return false
        }
        else if (value.size % 2 == 0) {
          for (i <- 0 to value.size - 1 by 2) {
            if (value(i) != first || value(i + 1) != second) {
              return false
            }
          }
          return true
        }
        else {
          for (i <- 0 to value.size - 2 by 2) {
            if (value(i) != first || value(i + 1) != second) {
              return false
            }
          }
          if (value(value.size - 1) != first) {
            return false
          }
          else {
            return true
          }
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val text = scan.next()

    val includes = includeCombinations()

    var max = 0
    includes.foreach { remove =>
      var str = new StringBuilder(text)
      str = str.filter(rec => remove.contains(rec))
      val pattern = str.toString()
      pattern match {
        case a if isPattern(a) => if (max < a.length) max = a.length
        case _ => max = max
      }
    }
    println(max)
  }
}
