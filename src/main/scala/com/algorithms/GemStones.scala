package com.algorithms

object GemStones {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val rocks = (0 until size).map(_ => scan.next())
    val gems = rocks.map(rock => {
      val arr = new Array[Boolean](26)
      rock.map(r => (r, r - 97)).foreach { case (_, b) => arr(b) = true }
      arr
    }).reduce((a, b) => a.zip(b).map{ case (a, b) => a && b })

    val real = gems.zipWithIndex.count { case (a, _) => a }
    println(real)
  }
}
