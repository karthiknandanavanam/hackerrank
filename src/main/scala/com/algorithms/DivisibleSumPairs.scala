package com.algorithms

object DivisibleSumPairs {

  def divisibleSumPairs(n: Int, d: Int, arr: Array[Int]): Int =  {
    var div = 0
    for (i <- 0 until n) {
      for (j <- i + 1 until n) {
        val sum = arr(i) + arr(j)
        if (sum % d == 0) {
          div = div + 1
        }
      }
    }
    div
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val d = scan.nextInt()

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    val div = divisibleSumPairs(arr.length, d, arr)
    println(div)
  }
}
