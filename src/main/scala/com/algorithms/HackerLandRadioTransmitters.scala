package com.algorithms

object HackerLandRadioTransmitters {

  def transmitters(hs: Array[Int], r: Int): Int = {
    val isDone = (0 until hs.length).map(_ => false)
    val h = hs.zip(isDone)

    def underRange(house: (Int, Boolean), mHouse: Int, r: Int) = {
      house._1 - r <= h(mHouse)._1 && h(mHouse)._1 <= house._1 + r
    }

    def nextTrans(a: Int) = {
      var i = a
      while (i < h.length && h(i)._2) i = i + 1
      var t = i
      while (t + 1 < h.length && underRange(h(t + 1), i, r)) t = t + 1
      t
    }

    def houseSignalConvert(c: Int) = {
      var i = c
      while (i >= 0 && underRange(h(c), i, r)) {
        h(i) = (h(i)._1, true)
        i = i - 1
      }
      var j = c
      while (j < h.length && underRange(h(c), j, r)) {
        h(j) = (h(j)._1, true)
        j = j + 1
      }
    }

    var count = 0
    var i = nextTrans(0)
    while (i < h.length && !h(i)._2) {
      houseSignalConvert(i)
      count = count + 1
      i = nextTrans(i)
    }
    count
  }

  def main(args: Array[String]): Unit = {
    val lines = scala.io.Source.fromInputStream(System.in).getLines()
    val in = lines.next().split(" ").map(r => r.toInt)
    val n = in(0)
    val k = in(1)

    val houses = lines.next().split(" ").map(r => r.toInt).sorted
    val num = transmitters(houses, k)
    println(num)
  }
}
