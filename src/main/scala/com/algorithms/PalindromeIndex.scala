package com.algorithms

object PalindromeIndex {

  def palindrome(str: String, i: Int, j: Int, skip: Boolean, index: Int): Int = {
    (i, j, skip) match {
      case (a, b, c) if a >= b && !c => -1
      case (a, b, _) if a >= b => index
      case (a, b, _) if str(a) == str(b) => palindrome(str, i + 1, j - 1, skip, index)
      case (a, b, c) if str(a) != str(b) && !c => palindrome(str, i + 1, j, true, i) + palindrome(str, i, j - 1, true, j) + 1
      case (a, b, c) if str(a) != str(b) && c => -1
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val input = (0 until size).map(_ => scan.next())
    val res = input.map(r => palindrome(r, 0, r.length - 1, false, -1))
    println(res.mkString("\n"))
  }
}
