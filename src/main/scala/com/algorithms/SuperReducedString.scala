package com.algorithms

object SuperReducedString {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val string = scan.next()

    var res = new StringBuilder(string)

    var i = 0
    while (i < res.size - 1) {
      val first = res.charAt(i)
      val second = res.charAt(i + 1)
      if (first == second) {
        res = res.deleteCharAt(i)
        res = res.deleteCharAt(i)
        i = 0
      }
      else {
        i = i + 1
      }
    }

    res match {
      case a if a.isEmpty => println("Empty String")
      case a => println(a)
    }
  }
}
