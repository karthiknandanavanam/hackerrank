package com.security

object SecurityPermutations {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val func = (1 to size).map(i => (i, scan.nextInt())).toMap

    val res = (1 to size).map(i => func(func(i))).mkString("\n")
    println(res)
  }
}
