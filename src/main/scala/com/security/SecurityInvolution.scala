package com.security

object SecurityInvolution {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val func = (1 to size).map(i => (i, scan.nextInt())).toMap
    if ((1 to size).map(i => (i, func(func(i)))).count(a => a._1 == a._2) == size) {
      println("YES")
    }
    else {
      println("NO")
    }
  }
}
