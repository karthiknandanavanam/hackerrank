package com.security

object SecurityFunctions {

  def calculate(a: Int) = a % 11

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val in = scan.nextInt()
    val res = calculate(in)
    println(res)
  }
}
