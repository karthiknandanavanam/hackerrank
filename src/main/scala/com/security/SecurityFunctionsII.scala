package com.security

object SecurityFunctionsII {

  def calculate(i: Int) = i * i

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val in = scan.nextInt()
    val res = calculate(in)
    println(res)
  }
}