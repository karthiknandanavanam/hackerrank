package com.security

object SecurityBijectiveFunctions {

  def isBijective(nums: Array[Int]) = {
    nums.sorted.zipWithIndex.map(r => (r._1, r._2 + 1)).count(r => r._1 == r._2) == nums.length
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val nums = (0 until size).map(_ => scan.nextInt()).toArray
    val res = isBijective(nums)
    println(if (res) "YES" else "NO")
  }
}
