package com.security

object SecurityFunctionInverses {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val inverses = (1 to size).map(a => (a, scan.nextInt())).map(r => (r._2, r._1)).sortWith((a, b) => a._1 < b._1)
    println(inverses.map(r => r._2).mkString("\n"))
  }
}
