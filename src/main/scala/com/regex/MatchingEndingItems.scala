package com.regex

import scala.io.Source

object MatchingEndingItems {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^[a-zA-Z]*s$"""
    println(input.matches(regex))
  }
}
