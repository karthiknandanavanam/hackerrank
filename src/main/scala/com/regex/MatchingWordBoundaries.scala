package com.regex

import scala.io.Source

object MatchingWordBoundaries {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """\b[aeiouAEIOU][a-zA-Z]*\b""".r
    println(regex.findAllMatchIn(input).mkString(", "))
  }
}
