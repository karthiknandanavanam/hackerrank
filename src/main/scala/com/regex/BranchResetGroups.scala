package com.regex

import scala.io.Source

object BranchResetGroups {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^[1-9]{2}((\-)|(:)|(\-\-\-)|(\.))[1-9]{2}\1[1-9]{2}\1[1-9]{2}$"""
    println(input.matches(regex))
  }
}
