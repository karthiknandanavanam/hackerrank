package com.regex

import scala.io.Source

object MatchingStartAndEnd {

  def main(args: Array[String]) = {
    val regex = """^\d\w\w\w\w\.$"""
    val input = Source.stdin.getLines().take(1).next()
    println(input.matches(regex))
  }
}
