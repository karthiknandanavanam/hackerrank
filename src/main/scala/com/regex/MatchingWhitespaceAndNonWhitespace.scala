package com.regex

object MatchingWhitespaceAndNonWhitespace {

  def main(args: Array[String]) = {
    val regex = """\S\S\s\S\S\s\S\S""".r
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()
    input match {
      case regex(_*) => println(true)
      case _ => println(false)
    }
  }
}
