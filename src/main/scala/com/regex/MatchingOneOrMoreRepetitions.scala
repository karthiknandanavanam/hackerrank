package com.regex

import scala.io.Source

object MatchingOneOrMoreRepetitions {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^\d+[A-Z]+[a-z]+$"""
    println(input.matches(regex))
  }
}
