package com.regex

import scala.io.Source

object MatchingSameTextAgainAndAgain {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^([a-z])(\w)(\s)(\W)(\d)(\D)([A-Z])([a-zA-Z])([aeiouAEIOU])(\S)\1\2\3\4\5\6\7\8\9\10$"""
    println(input.matches(regex))
  }
}
