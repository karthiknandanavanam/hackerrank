package com.regex

import scala.io.Source

object MatchingXRepetitions {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^[02468a-zA-Z]{40}[\s13579]{5}$"""
    println(input.matches(regex))
  }
}
