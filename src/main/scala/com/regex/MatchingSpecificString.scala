package com.regex

object MatchingSpecificString {

  def main(args: Array[String]) = {
    println("The hackerrank team is on a mission to flatten the world by restructuring the DNA of every company on the planet. We rank programmers based on their coding skills, helping companies source great programmers and reduce the time to hire. As a result, we are revolutionizing the way companies discover and evaluate talented engineers. The hackerrank platform is the destination for the best engineers to hone their skills and companies to find top engineers.")
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()

    val s = """hackerrank""".r
    println("Number of matches: " + s.findAllMatchIn(input).length)
  }
}