package com.regex

object MatchingAnythingButANewLine {

  def main(args: Array[String]) = {
    val s = """...\....\....\....""".r
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()
    input match {
      case s(_*) => println(true)
      case a => println(false)
    }
  }
}
