package com.regex

object MatchingWordAndNonWordCharacter {

  def main(args: Array[String]) = {
    val regex = """\w\w\w\W\w{10}\W\w{3}"""
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()
    println(input.matches(regex))
  }
}
