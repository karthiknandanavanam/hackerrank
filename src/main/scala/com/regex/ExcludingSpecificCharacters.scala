package com.regex

import scala.io.Source

object ExcludingSpecificCharacters {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().take(1).next()
    val regex = """^[^0-9][^aeiou][^bcDF]\S[^AEIOU][^\.,]$"""
    println(input.matches(regex))
  }
}
