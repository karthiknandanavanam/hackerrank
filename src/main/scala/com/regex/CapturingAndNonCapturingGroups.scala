package com.regex

import scala.io.Source

object CapturingAndNonCapturingGroups {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """(ok){3,}""".r
    println(regex.findAllMatchIn(input))
  }
}
