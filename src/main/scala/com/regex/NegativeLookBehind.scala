package com.regex

import scala.io.Source

object NegativeLookBehind {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """(?<![aeiouAEIOU]).""".r
    println(regex.findAllMatchIn(input).mkString(", "))
  }
}
