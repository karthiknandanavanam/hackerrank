package com.regex

import scala.io.Source

object MatchingSpecificCharacters {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().take(1).next()
    val regex = """^[123][120][xs0][30Aa][xsu][\.,]$"""
    println(input.matches(regex))
  }
}
