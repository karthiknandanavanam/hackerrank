package com.regex

import scala.io.Source

object MatchingCharacterRanges {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().take(1).next()
    val regex = """^[a-z][1-9][^a-z][^A-Z][A-Z].*"""
    println(input.matches(regex))
  }
}
