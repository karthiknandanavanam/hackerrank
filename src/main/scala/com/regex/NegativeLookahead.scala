package com.regex

import scala.io.Source

object NegativeLookahead {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """(.)(?!\1)""".r
    println(regex.findAllMatchIn(input).mkString(", "))
  }
}
