package com.regex

import scala.io.Source

object MatchingXYRepetitions {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^\d{1,2}[a-zA-Z]{3,}\.{0,3}$"""
    println(input.matches(regex))
  }
}
