package com.regex

import scala.io.Source

object PositiveLookahead {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """o(?=oo)""".r
    println(regex.findAllMatchIn(input).mkString(", "))
  }
}
