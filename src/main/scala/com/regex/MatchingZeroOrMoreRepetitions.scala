package com.regex

import scala.io.Source

object MatchingZeroOrMoreRepetitions {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^\d{2,}[a-z]*[A-Z]*$"""
    println(input.matches(regex))
  }
}
