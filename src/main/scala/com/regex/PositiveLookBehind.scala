package com.regex

import scala.io.Source

object PositiveLookBehind {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """(?<=[13579])\d""".r
    println(regex.findAllMatchIn(input).mkString(", "))
  }
}
