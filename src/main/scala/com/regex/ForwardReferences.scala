package com.regex

import scala.io.Source

object ForwardReferences {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^((\3tic)|(tac))+$"""
    println(input.matches(regex))
  }
}
