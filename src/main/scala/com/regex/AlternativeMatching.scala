package com.regex

import scala.io.Source

object AlternativeMatching {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^(Mr\.|Mrs\.|Ms\.|Dr\.|Er\.)([a-zA-Z]+)$"""
    println(input.matches(regex))
  }
}
