package com.regex

object MatchingDigitAndNonDigitCharacters {

  def main(args: Array[String]): Unit = {
    val regex = """\d\d\D\d\d\D\d\d\d\d""".r
    val scan = new java.util.Scanner(System.in)
    val input = scan.nextLine()
    input match {
      case regex(_*) => println(true)
      case a => println(false)
    }
  }
}
