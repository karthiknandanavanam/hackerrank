package com.regex

import scala.io.Source

object BackreferencesToFailedGroups {

  def main(args: Array[String]) = {
    val input = Source.stdin.getLines().next()
    val regex = """^[0-9]{2}(\-?)[0-9]{2}\1[0-9]{2}\1[0-9]{2}$"""
    println(input.matches(regex))
  }
}
