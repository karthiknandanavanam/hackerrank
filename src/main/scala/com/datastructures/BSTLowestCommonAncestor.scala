package com.datastructures

object BSTLowestCommonAncestor {

  trait Tree {
    def isEmpty = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.data
      }
    }

    def left = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.le
      }
    }

    def right = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.ri
      }
    }

    def setLeft(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.le = node
      }
    }

    def setRight(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.ri = node
      }
    }

    def lca(l: Int, r: Int): Tree = {
      var iter = this
      while (!iter.isEmpty) {
        if (iter.data < l) {
          iter = iter.right
        }
        else if (iter.data > r) {
          iter = iter.left
        }
        else {
          return iter
        }
      }
      iter
    }

    def insert(value: Int): Tree = {
      val node = new NonEmpty(value)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          var iter = this
          while (!iter.isEmpty) {
            if (node.data < iter.data && iter.left.isEmpty) {
              iter.setLeft(node)
              return this
            }
            else if (node.data > iter.data && iter.right.isEmpty) {
              iter.setRight(node)
              return this
            }
            else if (node.data < iter.data) {
              iter = iter.left
            }
            else if (node.data > iter.data) {
              iter = iter.right
            }
          }
          this
        }
      }
    }
  }

  class NonEmpty(a: Int) extends Tree {
    var le: Tree = new Empty
    var ri: Tree = new Empty
    override def data: Int = a
  }

  class Empty extends Tree {
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val l = scan.nextInt()
    val r = scan.nextInt()
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var bst: Tree = new Empty()
    for (i <- 0 until size) {
      bst = bst.insert(arr(i))
    }

    val lca = bst.lca(l, r)
    println(lca.data)
  }
}
