package com.datastructures

import scala.collection.mutable

object TreeTraversal {

  trait Tree {
    def data(): Int
    def setLeft(a: Tree)
    def left(): Tree
    def setRight(a: Tree)
    def right(): Tree
    def isEmpty(): Boolean

    def levelOrderTraversal(): String = {
      val queue = mutable.Queue[Tree]()
      queue.enqueue(this)

      var res = ""
      while (!queue.isEmpty) {
        val tree = queue.dequeue()
        res = res + tree.data()
        if (!tree.left.isEmpty()) queue.enqueue(tree.left())
        if (!tree.right.isEmpty()) queue.enqueue(tree.right())
      }
      res
    }

    def topView(): String = {
      var res = ""
      def le(a: Tree): Unit = {
        a match {
          case p: Empty => ""
          case p: NonEmpty => le(a.left()); res = res + a.data()
        }
      }

      def ri(a: Tree): Unit = {
        a match {
          case p: Empty => ""
          case p: NonEmpty => res = res + a.data(); ri(a.right())
        }
      }

      this match {
        case a: Empty => ""
        case a: NonEmpty => le(a.left()); res = res + a.data(); ri(a.right());
      }
      res
    }

    def height(): Int = {
      var max = Int.MinValue
      def rec(a: Tree, h: Int): Unit = {
        a match {
          case b: Empty => {
            if (max < h) {
              max = h
            }
          }
          case b: NonEmpty => {
            rec(b.left(), h + 1)
            rec(b.right(), h + 1)
          }
        }
      }
      rec(this, 0)
      max match {
        case p if p < 0 => p
        case _ => max - 1
      }
    }

    def add(a: Tree): Tree = {
      (this, a) match {
        case (t, v) if t.isEmpty() => a; return a
        case (t, v) if v.data < t.data && t.left.isEmpty() => this.setLeft(a)
        case (t, v) if v.data < t.data => this.left.add(v)
        case (t, v) if v.data > t.data && t.right().isEmpty() => this.setRight(a)
        case (t, v) if v.data > t.data => this.right.add(v)
        case (t, v) if v.data == t.data => ""
      }
      this
    }

    def preOrder() = {
      var res = ""
      def rec(a: Tree): Unit = {
        a match {
          case a if a.isEmpty => ""
          case a => res = res + a.data(); rec(a.left()); rec(a.right())
        }
      }
      rec(this)
      res
    }

    def inOrder() = {
      var res = ""
      def rec(a: Tree): Unit = {
        a match {
          case a if a.isEmpty() => ""
          case a => rec(a.left()); res = res + a.data(); rec(a.right())
        }
      }
      rec(this)
      res
    }

    def postOrder() = {
      var res = ""
      def rec(a: Tree): Unit = {
        a match {
          case a if a.isEmpty() => ""
          case a => rec(a.left()); rec(a.right()); res = res + a.data()
        }
      }
      rec(this)
      res
    }
  }

  class NonEmpty(d: Int) extends Tree {
    var l: Tree = new Empty
    var r: Tree = new Empty

    override def data(): Int = d
    override def setLeft(a: Tree) = l = a
    override def left(): Tree = l
    override def setRight(a: Tree) = r = a
    override def right(): Tree = r
    override def isEmpty(): Boolean = false
  }

  class Empty extends Tree {
    override def data(): Int = throw new UnsupportedOperationException
    override def setLeft(a: Tree): Unit = throw new UnsupportedOperationException
    override def left(): Tree = throw new UnsupportedOperationException
    override def setRight(a: Tree): Unit = throw new UnsupportedOperationException
    override def right(): Tree = throw new UnsupportedOperationException
    override def isEmpty(): Boolean = true
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var tree: Tree = new Empty
    for (i <- 0 until size) {
      val node = new NonEmpty(arr(i))
      tree = tree.add(node)
    }

    println(tree.preOrder())
    println(tree.inOrder())
    println(tree.postOrder())
    println(tree.height())
    println(tree.topView())
    println(tree.levelOrderTraversal())
  }
}
