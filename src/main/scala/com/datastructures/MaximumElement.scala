package com.datastructures

import scala.collection.mutable

object MaximumElement {

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val stack = mutable.Stack[(Int, Int)]()
    for (i <- 0 until size) {
      val q = scan.nextInt()
      q match {
        case 1 => {
          val num = scan.nextInt()
          if (stack.isEmpty) {
            stack.push((num, num))
          }
          else {
            val max = stack.head._2
            if (max < num) {
              stack.push((num, num))
            }
            else {
              stack.push((num, max))
            }
          }
        }
        case 2 => stack.pop()
        case 3 => println(stack.head._2)
      }
    }
  }
}
