package com.datastructures

import scala.collection.mutable

object LargestRectangle {

  def maxRectangle(arr: Array[Int]) = {

    def findRec(i: Int) = {
      var area = List[Int]()
      var s = List[Int]()
      for(j <- i until arr.length) {
        if (s.isEmpty) {
          s = arr(j) :: s
        }
        else if (s.head > arr(j)) {
          area = s.head * s.length :: area
          s = arr(j) :: s
        }
        else if (s.head <= arr(j)) {
          s = s.head :: s
        }
      }
      area = s.head * s.length :: area
      area.max
    }

    arr.indices.map { i =>
      val res = findRec(i)
      res
    }.union(arr).max
  }

  def largest(arr: Array[Int]) = {
    val pos = scala.collection.mutable.Stack[Int]()
    val heights = scala.collection.mutable.Stack[Int]()

    var max = Int.MinValue
    var i = 0
    while (i < arr.length) {
      if (pos.isEmpty) {
        pos.push(i)
        heights.push(arr(i))
      }
      else if (heights.head < arr(i)) {
        pos.push(i)
        heights.push(arr(i))
      }
      else if (heights.head > arr(i)) {
        var s = i
        while (heights.nonEmpty && heights.head > arr(i)) {
          val position = pos.pop()
          val height = heights.pop()
          val area = height * (i - position)
          if (max < area) max = area
          s = position
        }
        pos.push(s)
        heights.push(arr(i))
      }
      i = i + 1
    }

    while (pos.nonEmpty) {
      val position = pos.pop()
      val height = heights.pop()
      val area = height * (i - position)
      if (max < area) max = area
    }
    max
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = (0 until size).map(_ => scan.nextInt()).toArray
    val res = largest(arr)
    println(res)
  }
}
