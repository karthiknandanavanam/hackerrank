package com.datastructures

object LeftRotation {
  def leftRotation(arr: Array[Int], size: Int) = {
    val first = arr(0)
    for (i <- 1 until size) {
      arr(i - 1) = arr(i)
    }
    arr(size - 1) = first
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.next().toInt
    val rotations = scan.next().toInt

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.next().toInt
    }

    for (i <- 0 until rotations) {
      leftRotation(arr, size)
    }

    println(arr.mkString(" "))
  }
}
