package com.datastructures

object SimpleTextEditor {

  val ops = scala.collection.mutable.Stack[String]()

  def append(i: StringBuilder, w: String) = {
    ops.push(i.toString())
    i.append(w)
  }

  def delete(i: StringBuilder, k: Int) = {
    ops.push(i.toString())
    i.delete(i.length - k, i.length)
  }

  def print(i: StringBuilder, k: Int) = println(i.charAt(k - 1))

  def undo = new StringBuilder(ops.pop())

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextLine().toInt
    var s = new StringBuilder
    (0 until size).foreach {_ =>
      val in = scan.nextLine().split(" ")
      val op = in(0).toInt

      op match {
        case 1 => val value = in(1); append(s, value)
        case 2 => val value = in(1); delete(s, value.toInt)
        case 3 => val value = in(1); print(s, value.toInt)
        case 4 => s = undo
      }
    }
  }
}
