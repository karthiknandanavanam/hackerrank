package com.datastructures

object LinkedListNonRecursiveFull {
  object LinkedList {
    def findMerge(a: LinkedList, b: LinkedList) = {
      val aSize = a.size()
      val bSize = b.size()

      var small: LinkedList = null
      var big: LinkedList = null
      if (aSize > bSize) {
        small = b
        big = a
      }
      else {
        small = a
        big = b
      }
      val matcher = Math.abs(aSize - bSize)
      for (i <- 0 until matcher) {
        big = big.next()
      }

      while (small != big) {
        small = small.next()
        big = big.next()
      }

      small
    }
  }
  trait DoubleLinkedList {
    def value(): Int
    def display() = {
      var res = ""
      var iter = this
      while (!iter.isEmpty()) {
        res = res + iter.value() + " "
        iter = iter.next()
      }
      res
    }
    def reverse() = {
      if (this.isEmpty()) {
        this
      }
      else if (this.next().isEmpty()) {
        this
      }
      else {
        var prev = this
        var iter = this.next()
        prev.addNext(new DoubleEmpty)
        while (!iter.isEmpty()) {
          val temp = iter.next()
          iter.addNext(prev)
          prev.addPrev(iter)
          prev = iter
          iter = temp
        }
        prev
      }
    }
    def addLink(a: DoubleLinkedList): DoubleLinkedList = {
      this.addNext(a)
      if (!a.isEmpty()) {
        a.addPrev(this)
      }
      this
    }
    def addNext(a: DoubleLinkedList)
    def addPrev(a: DoubleLinkedList)
    def next(): DoubleLinkedList
    def prev(): DoubleLinkedList
    def isEmpty(): Boolean
    def addSorted(data: Int): DoubleLinkedList = {
      val node: DoubleLinkedList = new DoubleNonEmpty(data)
      if (this.isEmpty()) {
        return node
      }
      else if (this.value() >= node.value()) {
        node.addNext(this)
        this.addPrev(node)
        return node
      }
      else {
        var prev = this
        var iter = this.next()
        while (!iter.isEmpty() && iter.value() < node.value()) {
          prev = iter
          iter = iter.next()
        }
        if (iter.isEmpty()) {
          prev.addNext(node)
          node.addPrev(prev)
          return this
        }
        else {
          prev.addNext(node)
          node.addPrev(prev)
          node.addNext(iter)
          iter.addPrev(node)
          return this
        }
      }
    }
  }

  class DoubleNonEmpty(a: Int) extends DoubleLinkedList {
    var ne: DoubleLinkedList = new DoubleEmpty
    var pr: DoubleLinkedList = new DoubleEmpty
    override def value(): Int = a
    override def next(): DoubleLinkedList = ne
    override def prev(): DoubleLinkedList = pr
    override def isEmpty(): Boolean = false
    override def addNext(a: DoubleLinkedList): Unit = this.ne = a
    override def addPrev(a: DoubleLinkedList): Unit = this.pr = a
  }

  class DoubleEmpty extends DoubleLinkedList {
    override def value(): Int = throw new UnsupportedOperationException
    override def addLink(a: DoubleLinkedList): DoubleLinkedList = throw new UnsupportedOperationException
    override def next(): DoubleLinkedList = throw new UnsupportedOperationException
    override def prev(): DoubleLinkedList = throw new UnsupportedOperationException
    override def isEmpty(): Boolean = true
    override def addNext(a: DoubleLinkedList): Unit = throw new UnsupportedOperationException
    override def addPrev(a: DoubleLinkedList): Unit = throw new UnsupportedOperationException
  }

  trait LinkedList {
    def value(): Int
    def addLink(a: LinkedList): LinkedList
    def next(): LinkedList
    def isEmpty(): Boolean
    def addEnd(a: LinkedList) = {
      var prev: LinkedList = new Empty
      var iter = this
      while (!iter.isEmpty()) {
        prev = iter
        iter = iter.next()
      }
      prev.addLink(a)
    }
    def isCycle(): Boolean = {
      var iter = this
      var iter2 = this.next()
      while (!iter.isEmpty() && !iter2.isEmpty()) {
        if (iter == iter2) {
          return true
        }
        iter = iter.next()
        if (!iter2.isEmpty() && !iter2.next().isEmpty()) {
          iter2 = iter2.next().next()
        }
        else {
          iter2 = new Empty
        }
      }
      false
    }
    def linkCycle(n: Int): LinkedList = {
      var prev: LinkedList = new Empty
      var iter = this
      while (!iter.isEmpty()) {
        prev = iter
        iter = iter.next()
      }

      var it = this
      for (i <- 0 until n) {
        it = it.next()
      }
      prev.addLink(it)
      this
    }
    def size(): Int = {
      var s = 0
      var iter = this
      while (!iter.isEmpty()) {
        s = s + 1
        iter = iter.next()
      }
      s
    }
    def deleteDuplicateValues() = {
      if (this.isEmpty()) {
        this
      }
      else if (this.next().isEmpty()) {
        this
      }
      else {
        var prev = this
        var iter = this.next()
        while (!iter.isEmpty()) {
          if (prev.value() == iter.value()) {
            prev.addLink(iter.next())
            iter = iter.next()
          }
          else {
            prev = iter
            iter = iter.next()
          }
        }
        this
      }
    }
    def getNode(pos: Int): Int = {
      val s = this.size()
      val start = s - pos - 1
      var node = this
      var iter = 0
      while (iter != start) {
        node = node.next()
        iter = iter + 1
      }
      node.value()
    }
    def merge(that: LinkedList): LinkedList = {
      if (this.isEmpty()) {
        return that
      }
      else if (that.isEmpty()) {
        return this
      }
      else {
        var mainHead = this
        var prev: LinkedList = new Empty
        var iterA = this
        var iterB = that
        while (!iterA.isEmpty() && !iterB.isEmpty()) {
          if (iterA.value() <= iterB.value()) {
            prev = iterA
            iterA = iterA.next()
          }
          else {
            val node = iterB
            iterB = iterB.next()
            if (prev.isEmpty()) {
              node.addLink(iterA)
              mainHead = node
              prev = mainHead
            }
            else {
              prev.addLink(node)
              node.addLink(iterA)
              prev = node
            }
          }
        }
        if (iterA.isEmpty() && iterB.isEmpty()) {
          return mainHead
        }
        else if (!iterA.isEmpty() && iterB.isEmpty()) {
          return mainHead
        }
        else if (iterA.isEmpty() && !iterB.isEmpty()) {
          prev.addLink(iterB)
          return mainHead
        }
        else {
          return mainHead
        }
      }
    }
    def addSorted(n: Int): LinkedList = {
      val node = new NonEmpty(n)
      node.addLink(new Empty)
      if (this.isEmpty()) {
        node
      }
      else if (this.next().isEmpty()) {
        if (n > this.value()) {
          this.addLink(node)
          return this
        }
        else {
          node.addLink(this)
          return node
        }
      }
      else {
        var prev: LinkedList = new Empty
        var iter = this
        while (!iter.isEmpty() && iter.value() <= n) {
          prev = iter
          iter = iter.next()
        }
        if (prev.isEmpty()) {
          node.addLink(this)
          return node
        }
        else if (iter.isEmpty()) {
          prev.addLink(node)
          return this
        }
        else {
          prev.addLink(node)
          node.addLink(iter)
          return this
        }
      }
    }
    def compare(that: LinkedList): Int = {
      var one = this
      var two = that

      while (!one.isEmpty() && !two.isEmpty()) {
        if (one.value() != two.value()) {
          return 0
        }
        one = one.next()
        two = two.next()
      }
      if (one.isEmpty() && two.isEmpty()) {
        1
      }
      else {
        0
      }
    }
    def reverse() = {
      if (this.isEmpty()) {
        this
      }
      else {
        val last = this
        var prev = this
        var iter = this.next()
        while (!iter.isEmpty()) {
          var temp = iter.next()
          iter.addLink(prev)
          prev = iter
          iter = temp
        }
        last.addLink(new Empty)
        prev
      }
    }
    def displayInReverse() = {
      def recursive(a: LinkedList): String = {
        a match {
          case r if r.isEmpty() => ""
          case _ => recursive(a.next()) + " " + a.value()
        }
      }
      recursive(this).trim
    }
    def delete(pos: Int) = {
      if (this.isEmpty()) {
        this
      }
      else if (pos == 0) {
        this.next()
      }
      else {
        var index = 0
        var iter = this
        while (index < pos - 1) {
          index = index + 1
          iter = iter.next()
        }
        val prev = iter
        val remove = prev.next()
        iter.addLink(iter.next().next())
        this
      }
    }
    def addBegin(a: Int) = {
      val node = new NonEmpty(a)
      node.link = this
      node
    }
    def addEnd(a: Int) = {
      val node = new NonEmpty(a)
      node.link = new Empty

      if (this.isInstanceOf[Empty]) {
        node
      }
      else {
        var iter = this
        while (!iter.next().isEmpty()) {
          iter = iter.next()
        }
        iter.addLink(node)
        this
      }
    }
    def addPos(a: Int, pos: Int): LinkedList = {
      val res = new NonEmpty(a)
      res.link = new Empty
      if (this.isEmpty()) {
        res
      }
      else {
        var iter = 0
        var node = this
        while (iter < pos - 1) {
          node = node.next()
          iter = iter + 1
        }
        val prev = node
        val next = node.next()
        prev.addLink(res)
        res.addLink(next)
        this
      }
    }
    def display(): String = {
      var iter = this
      var dis = new String
      while (!iter.isEmpty()) {
        dis = dis + iter.value + " "
        iter = iter.next()
      }
      dis
    }
  }

  class Empty extends LinkedList {
    override def isEmpty(): Boolean = true
    override def addLink(a: LinkedList): LinkedList = a
    override def next(): LinkedList = throw new UnsupportedOperationException
    override def value(): Int = throw new UnsupportedOperationException
  }

  class NonEmpty(i: Int) extends LinkedList {
    var link: LinkedList = null
    override def isEmpty(): Boolean = false
    override def addLink(a: LinkedList): LinkedList = {
      this.link = a
      this
    }
    override def next(): LinkedList = link
    override def value(): Int = i
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    def insertNodeAtHead = {
      val size = scan.nextInt()

      var lb: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        lb = lb.addBegin(num)
      }
      println(lb.display())
    }

    def insertNodeAtTail = {
      val size = scan.nextInt()

      var le: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        le = le.addEnd(num)
      }
      println(le.display())
    }

    def insertAtSpecific = {
      val size = scan.nextInt()

      var ls: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        val pos = scan.nextInt()
        ls = ls.addPos(num, pos)
      }
      println(ls.display())
    }

    def deleteAtPos = {
      val size = scan.nextInt()

      var ls: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        ls = ls.addBegin(num)
      }
      val pos = scan.nextInt()
      println(ls.display())
      val res = ls.delete(pos)
      println(res.display())
    }

    def printInReverse = {
      val size = scan.nextInt()

      var ls: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        ls = ls.addBegin(num)
      }
      println(ls.displayInReverse())
    }

    def reverseLinkedList = {
      val size = scan.nextInt()

      var ls: LinkedList = new Empty
      for (i <- 0 until size) {
        val num = scan.nextInt()
        ls = ls.addBegin(num)
      }
      println(ls.display())
      val reverse = ls.reverse()
      println(reverse.display())
    }

    def compareLinkedList = {
      val sizeOne = scan.nextInt()

      var one: LinkedList = new Empty
      for (i <- 0 until sizeOne) {
        val num = scan.nextInt()
        one = one.addBegin(num)
      }

      val sizeTwo = scan.nextInt()
      var two: LinkedList = new Empty
      for (i <- 0 until sizeTwo) {
        val num = scan.nextInt()
        two = two.addBegin(num)
      }
      println(one.display())
      println(two.display())
      println(one.compare(two))
    }

    def sortedLinkedList = {
      val sizeOne = scan.nextInt()

      var one: LinkedList = new Empty
      for (i <- 0 until sizeOne) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }

      val sizeTwo = scan.nextInt()
      var two: LinkedList = new Empty
      for (i <- 0 until sizeTwo) {
        val num = scan.nextInt()
        two = two.addSorted(num)
      }
      println(one.display())
      println(two.display())
      val merge = one.merge(two)
      println(merge.display)
    }

    def getNode = {
      val sizeOne = scan.nextInt()

      var one: LinkedList = new Empty
      for (i <- 0 until sizeOne) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }

      println(one.display())

      val pos = scan.nextInt()
      val res = one.getNode(pos)
      println(res)
    }

    def deleteDuplicates = {
      val sizeOne = scan.nextInt()

      var one: LinkedList = new Empty
      for (i <- 0 until sizeOne) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }

      println(one.display())
      val dedup = one.deleteDuplicateValues()
      println(dedup.display())
    }

    def linkCycle = {
      val sizeOne = scan.nextInt()

      var one: LinkedList = new Empty
      for (i <- 0 until sizeOne) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }

      val link = scan.nextInt()

      println(one.isCycle())
    }

    def twoMerges = {
      val mergeSize = scan.nextInt()
      var merge: LinkedList = new Empty
      for (i <- 0 until mergeSize) {
        val num = scan.nextInt()
        merge = merge.addSorted(num)
      }

      val oneSize = scan.nextInt()
      var one: LinkedList = new Empty
      for (i <- 0 until oneSize) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }

      val twoSize = scan.nextInt()
      var two: LinkedList = new Empty
      for (i <- 0 until twoSize) {
        val num = scan.nextInt()
        two = two.addSorted(num)
      }

      one.addEnd(merge)
      two.addEnd(merge)

      val mergeRes = LinkedList.findMerge(one, two)
      println(merge.display())
      println(mergeRes.display())
    }

    def doubleLinkedList = {
      val oneSize = scan.nextInt()
      var one: DoubleLinkedList = new DoubleEmpty
      for (i <- 0 until oneSize) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }
      println(one.display())
    }

    def doubleLinkedListReverse = {
      val oneSize = scan.nextInt()
      var one: DoubleLinkedList = new DoubleEmpty
      for (i <- 0 until oneSize) {
        val num = scan.nextInt()
        one = one.addSorted(num)
      }
      println(one.display())
      one = one.reverse()
      println(one.display())
    }

    doubleLinkedListReverse
  }
}
