package com.datastructures

object JesseAndCookies {

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val minSum = scan.nextInt()

    val heap = scala.collection.mutable.PriorityQueue.empty(implicitly[Ordering[Int]].reverse)
    (0 until size).map(_ => scan.nextInt()).foreach(r => heap.enqueue(r))

    var count = 0
    while (heap.nonEmpty && heap.head < minSum && heap.length >= 2) {
      val i = heap.dequeue()
      val j = heap.dequeue()
      val n = 1 * i + 2 * j
      heap.enqueue(n)
      count = count + 1
    }

    val res = heap match {
      case a if a.isEmpty => -1
      case a if a.length == 1 && a.head < minSum => -1
      case a if a.nonEmpty && a.head >= minSum => count
    }
    println(res)
  }
}
