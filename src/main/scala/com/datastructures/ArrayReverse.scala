package com.datastructures

object ArrayReverse {

  def reverseInPlace(array: Array[Int]) = {
    val len = array.length

    var i = 0
    var j = len - 1
    while (i < j) {
      val temp = array(i)
      array(i) = array(j)
      array(j) = temp
      i += 1
      j -= 1
    }
    array
  }

  def reverse(array: Array[Int]) = {
    val len = array.size
    val rev = new Array[Int](len)

    var index = 0
    for (i <- array.size - 1 to 0 by -1) {
      rev(index) = array(i)
      index = index + 1
    }
    rev
  }

  def main(args: Array[String]) = {
    val scanner = new java.util.Scanner(System.in)
    val size = scanner.next().toInt

    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      val num = scanner.next().toInt
      arr(i) = num
    }

    val rev = reverseInPlace(arr)
    println(rev.mkString(" "))
  }
}
