package com.datastructures

object QueueUsingTwoStacks {

  class Queue {
    val enq = scala.collection.mutable.Stack[Int]()
    val deq = scala.collection.mutable.Stack[Int]()

    def enqueue(a: Int) = {
      enq.push(a)
    }

    def dequeue(): Int = {
      deq match {
        case a if a.nonEmpty => a.pop()
        case a if a.isEmpty => {
          while (enq.nonEmpty) deq.push(enq.pop())
          deq.pop()
        }
      }
    }

    def head(): Int = {
      deq match {
        case a if a.nonEmpty => a.head
        case a if a.isEmpty => {
          while (enq.nonEmpty) deq.push(enq.pop())
          deq.head
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val oSize = scan.nextInt()
    val que = new Queue
    for (i <- 0 until oSize) {
      val op = scan.nextInt()
      op match {
        case 1 => val num = scan.nextInt(); que.enqueue(num)
        case 2 => que.dequeue()
        case 3 => println(que.head())
      }
    }
  }
}
