package com.datastructures

object DynamicArray {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val listSize = scan.next().toInt
    val inputCount = scan.next().toInt

    val arr = new Array[List[Int]](listSize)
    for (i <- 0 until listSize) {
      arr(i) = List[Int]()
    }

    var lastAnswer = 0

    for (i <- 0 until inputCount) {
      val typ = scan.next().toInt
      val x = scan.next().toInt
      val y = scan.next().toInt
      val in = (typ, x, y)
      if (typ == 1) {
        val pos = (x ^ lastAnswer) % listSize
        arr(pos) = (y :: arr(pos).reverse).reverse
      }
      else if (typ == 2) {
        val pos = (x ^ lastAnswer) % listSize
        lastAnswer = arr(pos)(y % arr(pos).length)
        println(lastAnswer)
      }
    }
  }
}
