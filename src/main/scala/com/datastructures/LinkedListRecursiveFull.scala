package com.datastructures

object LinkedListRecursiveFull {

  trait LinkedList {
    def empty = {
      this match {
        case Empty => true
        case NonEmpty(a) => false
      }
    }

    def data = {
      this match {
        case Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.dat
      }
    }

    def link(node: LinkedList) = {
      this match {
        case Empty => node
        case a: NonEmpty => a.next = node
      }
    }

    def front(node: LinkedList) = {
      this match {
        case Empty => node
        case a: NonEmpty => node.link(a); node
      }
    }

    def tail(node: LinkedList): LinkedList = {
      this match {
        case Empty => node
        case a: NonEmpty if a.next.empty => a.next = node; this
        case a: NonEmpty => a.next.tail(node); this
      }
    }

    def insert(node: LinkedList, pos: Int): LinkedList = {
      (this, pos) match {
        case (_, 0) => node.link(this); node
        case (a: NonEmpty, n) if n > 0 => this.link(a.next.insert(node, pos - 1)); this
      }
    }

    def delete(pos: Int): LinkedList = {
      (this, pos) match {
        case (Empty, _) => this
        case (n: NonEmpty, c) if c > 0 => n.link(n.next.delete(c - 1)); this
        case (n: NonEmpty, 0) => n.next
      }
    }

    def reverse: LinkedList = {
      this match {
        case Empty => Empty
        case a: NonEmpty => val rev = a.next.reverse; rev.link(a); rev
      }
    }

    def display: String = {
      this match {
        case Empty => ""
        case a: NonEmpty => a.dat + " " + a.next.display
      }
    }

    def displayReverse: String = {
      this match {
        case Empty => ""
        case a: NonEmpty => a.next.displayReverse + " " + a.data
      }
    }
  }

  case object Empty extends LinkedList {
  }

  case class NonEmpty(val dat: Int) extends LinkedList {
    var next: LinkedList = Empty
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var ll: LinkedList = Empty
    for (i <- 0 until size) {
      val num = arr(i)
      val node = NonEmpty(num)
      ll = ll.front(node)
    }
    println("forward display: " + ll.display)
    println("reverse display: " + ll.displayReverse)
    ll = ll.reverse
    println("forward display: " + ll.display)

    var llTail: LinkedList = Empty
    for (i <- 0 until size) {
      val num = arr(i)
      val node = NonEmpty(num)
      llTail = llTail.tail(node)
    }
    println(llTail.display)

    print("Enter Position insert: ")
    val pos = scan.nextInt()
    print("Enter Node: ")
    val node = new NonEmpty(scan.nextInt())
    llTail = llTail.insert(node, pos)
    println(llTail.display)

    print("Enter Position delete:")
    val del = scan.nextInt()
    llTail.delete(del)
    print(llTail.display)
  }
}
