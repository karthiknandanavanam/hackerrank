package com.datastructures

object Array2DHourGlass {
  def main(args: Array[String]) = {
    val m = Array.ofDim[Int](6, 6)
    val scan = new java.util.Scanner(System.in)

    for (i <- 0 until 6) {
      for (j <- 0 until 6) {
        m(i)(j) = scan.next().toInt
      }
    }

    var max = Int.MinValue
    for (i <- 0 to 3) {
      for (j <- 0 to 3) {
        val top = m(i)(j) + m(i)(j + 1) + m(i)(j + 2)
        val mid = m(i + 1)(j + 1)
        val bottom = m(i + 2)(j) + m(i + 2)(j + 1) + m(i + 2)(j + 2)
        val total = top + mid + bottom
        if (max < total) {
          max = total
        }
      }
    }

    println(max)
  }
}
