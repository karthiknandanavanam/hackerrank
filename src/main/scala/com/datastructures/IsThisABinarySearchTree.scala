package com.datastructures

import scala.collection.mutable

/*
3 5 2 1 4 6
4 2 6 1 3 5 7
 */
object IsThisABinarySearchTree {

  trait Tree {
    def left = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.le
    }

    def right = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.ri
    }

    def addLeft(n: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.le = n
    }

    def addRight(n: Tree) = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.ri = n
    }

    def isEmpty = this match {
      case Empty => true
      case a: NonEmpty => false
    }

    def data = this match {
      case Empty => throw new UnsupportedOperationException()
      case a: NonEmpty => a.a
    }

    def inorder: List[Int] = this match {
      case Empty => List()
      case a: NonEmpty => a.left.inorder ++ List(a.data) ++ a.right.inorder
    }

    def isBST: Boolean = {
      val res = this.inorder.toArray
      var i = 1
      while (i < res.length) {
        if (res(i) < res(i - 1)) {
          return false
        }
        i = i + 1
      }
      true
    }
  }

  case class NonEmpty(a: Int) extends Tree {
    var le: Tree = Empty
    var ri: Tree = Empty
  }

  case object Empty extends Tree {
  }

  def main(args: Array[String]) = {
    def insert(n: List[Int], t: mutable.Queue[Tree], res: Tree): Tree = {
      (n, t) match {
        case (a, _) if a.isEmpty => res
        case (a, b) if a.nonEmpty && b.isEmpty => val node = NonEmpty(a.head); t.enqueue(node); insert(a.tail, t, node)
        case (a, b) if a.nonEmpty && b.nonEmpty && b.head.left.isEmpty => val node = NonEmpty(a.head); t.head.addLeft(node); t.enqueue(node); insert(a.tail, t, res)
        case (a, b) if a.nonEmpty && b.nonEmpty && b.head.right.isEmpty => val node = NonEmpty(a.head); t.head.addRight(node); t.enqueue(node); insert(a.tail, t, res)
        case (a, b) if a.nonEmpty && b.nonEmpty && !b.head.left.isEmpty && !b.head.right.isEmpty => val node = NonEmpty(a.head); t.dequeue(); t.enqueue(node); t.head.addLeft(node); insert(a.tail, t, res)
      }
    }

    val arr = scala.io.Source.stdin.getLines().next().split(" ").map(r => r.toInt).toList
    val tree = insert(arr, mutable.Queue(), Empty)

    println(if (tree.isBST) "Yes" else "No")
  }
}
