package com.datastructures

object SwapNodesAlgo {

  trait Tree {
    def isEmpty = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def setLeft(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.le = node
      }
    }

    def setRight(node: Tree) = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.ri = node
      }
    }

    def left = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.le
      }
    }

    def right = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.ri
      }
    }

    def swap(h: Int) = {
      def recur(node: Tree, c: Int): Unit = {
        (node, c) match {
          case (n: Empty, a) =>
          case (n: NonEmpty, a) if a % h == 0 => val temp = n.le; n.le = n.ri; n.ri = temp; recur(n.le, a + 1); recur(n.ri, a + 1)
          case (n: NonEmpty, a) => recur(n.le, a + 1); recur(n.ri, a + 1)
        }
      }

      recur(this, 1)
    }

    def data: Int = {
      this match {
        case a: Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.data
      }
    }

    def height: Int = {
      def recur(node: Tree, h: Int): Int = {
        node match {
          case a: Empty => h - 1
          case a: NonEmpty => {
            val lH = recur(a.left, h + 1)
            val rH = recur(a.right, h + 1)
            Math.max(lH, rH)
          }
        }
      }
      recur(this, 1)
    }

    def inorder: String = {
      this match {
        case a: Empty => ""
        case a: NonEmpty => a.le.inorder + a.data + " " + a.ri.inorder
      }
    }
  }

  class Empty extends Tree {
  }

  class NonEmpty(a: Int) extends Tree {
    var le: Tree = new Empty
    var ri: Tree = new Empty
    override def data: Int = a
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val root = new NonEmpty(1)
    val queue = scala.collection.mutable.Queue(root)
    for (i <- 0 until size) {
      val left = scan.nextInt()
      val right = scan.nextInt()
      val parent = queue.dequeue()
      if (left != -1) {
        val le = new NonEmpty(left)
        parent.le = le
        queue.enqueue(le)
      }
      if (right != -1) {
        val ri = new NonEmpty(right)
        parent.ri = ri
        queue.enqueue(ri)
      }
    }

    val hSize = scan.nextInt()
    for (i <- 0 until hSize) {
      val h = scan.nextInt()
      root.swap(h)
      println(root.inorder)
    }
  }
}
