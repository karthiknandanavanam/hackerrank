package com.datastructures

import scala.collection.mutable

object BalancedBrackets {

  def isBalanced(str: String): Boolean = {
    val stack = mutable.Stack[Char]()
    for (c <- str) {
      (c, stack)match {
        case ('{', _) => stack.push(c)
        case ('(', _) => stack.push(c)
        case ('[', _) => stack.push(c)
        case ('}', a) if a.isEmpty || a.head != '{' => return false
        case (']', a) if a.isEmpty || a.head != '[' => return false
        case (')', a) if a.isEmpty || a.head != '(' => return false
        case ('}', a) if a.head == '{' => stack.pop()
        case (']', a) if a.head == '[' => stack.pop()
        case (')', a) if a.head == '(' => stack.pop()
      }
    }
    stack match {
      case a if a.isEmpty => true
      case _ => false
    }
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[String](size)
    for (i <- 0 until size) {
      arr(i) = scan.next()
    }

    for (i <- 0 until size) {
      if (isBalanced(arr(i))) {
        println("YES")
      }
      else {
        println("NO")
      }
    }
  }
}
