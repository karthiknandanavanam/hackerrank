package com.datastructures

object BinarySearchTreeInsertion {

  trait Tree {
    def data(): Int
    def addRight(a: Tree)
    def addLeft(a: Tree)
    def right: Tree
    def left: Tree
    def isEmpty: Boolean = {
      this match {
        case a: Empty => true
        case a: NonEmpty => false
      }
    }

    def inorder: String = {
      var res = ""
      def rec(a: Tree): Unit = {
        a match {
          case a: Empty => res = res
          case a: NonEmpty => {
            rec(a.le)
            res = res + a.data()
            rec(a.ri)
          }
        }
      }
      rec(this)
      res
    }

    def insert(a: Int) = {
      val node = new NonEmpty(a)
      this match {
        case a: Empty => node
        case a: NonEmpty => {
          var iter = this
          while (!iter.isEmpty) {
            if (iter.data() < node.data() && !iter.right.isEmpty) {
              iter = iter.right
            }
            else if (iter.data() > node.data() && !iter.left.isEmpty) {
              iter = iter.left
            }
            else if (iter.data() < node.data() && iter.right.isEmpty) {
              iter.addRight(node)
              iter = new Empty
            }
            else if (iter.data() > node.data() && iter.left.isEmpty) {
              iter.addLeft(node)
              iter = new Empty
            }
            else if (iter.data == node.data) {
              iter = new Empty
            }
          }
          this
        }
      }
    }
  }

  class NonEmpty(a: Int) extends Tree {
    var le: Tree = new Empty
    var ri: Tree = new Empty
    override def data(): Int = a
    override def addRight(a: Tree): Unit = ri = a
    override def addLeft(a: Tree): Unit = le = a
    override def right: Tree = ri
    override def left: Tree = le
  }

  class Empty extends Tree {
    override def data(): Int = throw new UnsupportedOperationException
    override def addRight(a: Tree): Unit = throw new UnsupportedOperationException
    override def addLeft(a: Tree): Unit = throw new UnsupportedOperationException
    override def right: Tree = throw new UnsupportedOperationException
    override def left: Tree = throw new UnsupportedOperationException
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()
    val arr = new Array[Int](size)
    for (i <- 0 until size) {
      arr(i) = scan.nextInt()
    }

    var bst: Tree = new Empty
    arr.foreach {
      rec => bst = bst.insert(rec)
    }

    println(bst.inorder)
  }
}
