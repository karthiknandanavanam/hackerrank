package com.datastructures

object GameOfTwoStacks {

  def game(aL: Array[Int], bL: Array[Int], sum: Int): Int = {
    var s = 0

    var i = 0
    while (i < aL.length && s + aL(i) <= sum) {
      val include = aL(i)
      s = s + include
      i = i + 1
    }

    var max = i
    var j = 0
    while (j < bL.length && i >= 0) {
      s = s + bL(j)
      j = j + 1
      while (s > sum && i > 0) {
        i = i - 1
        s = s - aL(i)
      }
      if (s <= sum && i + j > max) max = i + j
    }
    max
  }

  def main(args: Array[String]): Unit = {
    val scan = scala.io.Source.fromInputStream(System.in).getLines().flatMap(r => r.split(" "))
    val size = scan.next().toInt

    (0 until size).map {_ =>
      val aSize = scan.next().toInt
      val bSize = scan.next().toInt
      val sum = scan.next().toInt

      val aStack = (0 until aSize).map(_ => scan.next().toInt).toArray
      val bStack = (0 until bSize).map(_ => scan.next().toInt).toArray
      val res = game(aStack, bStack, sum)
      println(res)
    }
  }
}
