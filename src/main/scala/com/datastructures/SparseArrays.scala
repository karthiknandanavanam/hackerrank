package com.datastructures

object SparseArrays {
  abstract class Trie {
    def include(a: String)
    def char: Char
    def count: Int
    def links: scala.collection.mutable.Set[Trie]
    def increment
    def query(a: String): Int
    def findLink(a: Char): Option[Trie]
  }

  class Node(c: Char) extends Trie {
    var cnt = 0
    val set = scala.collection.mutable.Set[Trie]()
    override def char: Char = c
    override def links: scala.collection.mutable.Set[Trie] = set
    override def count: Int = cnt
    override def increment = cnt += 1

    override def findLink(a: Char): Option[Trie] = {
      val tries = set.filter(rec => rec.char == a).toList
      tries match {
        case tries if tries.isEmpty => Option.empty
        case tries => Option(tries.head)
      }
    }

    override def query(a: String): Int = {
      if (a.isEmpty) {
        count
      }
      else {
        val head = a.head
        val tail = a.tail

        val validLinks = findLink(head)
        validLinks match {
          case v if v.isEmpty => 0
          case v => validLinks.get.query(tail)
        }
      }
    }

    override def include(a: String) = {
      if (a.isEmpty) {
        this.increment
      }
      else {
        val ch = a.head
        val tail = a.tail
        val validLinks = findLink(ch)

        validLinks match {
          case v if v.isEmpty => val trie = new Node(ch)
            set.add(trie)
            trie.include(tail)
          case v => v.get.include(tail)
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val root: Trie = new Node('\0')

    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    for (i <- 1 to size) {
      val string = scan.next()
      root.include(string)
    }

    val querySize = scan.nextInt()
    val arr = new Array[Int](querySize)
    for (i <- 0 until querySize) {
      val string = scan.next()
      val count = root.query(string)
      arr(i) = count
    }

    println(arr.mkString("\n"))
  }

}
