package com.datastructures
import java.util.Scanner

object EqualStacks {

  def minimum(a: Int, b: Int, c: Int): Int = {
    Array(a, b, c).min
  }

  def stack(scan: Scanner, iSize: Int) = {
    val arrI = new Array[Int](iSize)
    for (i <- 0 until iSize) {
      arrI(i) = scan.nextInt()
    }

    var sum = 0
    val stack = scala.collection.mutable.Stack[(Int, Int)]()
    for (i <- iSize - 1 to 0 by -1) {
      val num = arrI(i)
      sum = sum + num
      stack.push((num, sum))
    }
    stack
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val iSize = scan.nextInt()
    val jSize = scan.nextInt()
    val kSize = scan.nextInt()

    val iStack = stack(scan, iSize)
    val jStack = stack(scan, jSize)
    val kStack = stack(scan, kSize)

    while (iStack.nonEmpty && jStack.nonEmpty && kStack.nonEmpty && !(iStack.head._2 == jStack.head._2 && jStack.head._2 == kStack.head._2)) {
      val min = minimum(iStack.head._2, jStack.head._2, kStack.head._2)
      if (iStack.head._2 > min) {
        iStack.pop()
      }
      if (jStack.head._2 > min) {
        jStack.pop()
      }
      if (kStack.head._2 > min) {
        kStack.pop()
      }
    }

    if (iStack.isEmpty || jStack.isEmpty || kStack.isEmpty) {
      println("0")
    }
    else {
      println(iStack.head._2)
    }
  }
}
