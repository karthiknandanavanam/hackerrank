package com.datastructures

object HuffmanDecoding {

  trait Tree {
    def isEmpty: Boolean = {
      this match {
        case a: Node => throw new UnsupportedOperationException()
        case a: Leaf => false
        case a: Empty => true
      }
    }

    def char: Char = {
      this match {
        case a: Node => throw new UnsupportedOperationException()
        case a: Leaf => a.char
      }
    }

    def count: Int = {
      this match {
        case a: Node => a.sum
        case a: Leaf => a.count
      }
    }

    def left: Tree = {
      this match {
        case a: Node => a.le
        case a: Leaf => throw new UnsupportedOperationException()
      }
    }

    def right: Tree = {
      this match {
        case a: Node => a.ri
        case a: Leaf => throw new UnsupportedOperationException()
      }
    }

    def insert(leafs: List[Tree]): Tree = {
      var iter = leafs.sortWith((a, b) => a.count < b.count)
      while (iter.length != 1) {
        val first = iter.head
        val second = iter.tail.head
        val node = new Node()
        node.le = first
        node.ri = second
        node.sum = first.count + second.count
        iter = iter.tail.tail
        iter = node :: iter
        iter = iter.sortWith((a, b) => a.count < b.count)
      }
      iter.head
    }

    def fit(in: String): Tree = {
      val res = in.map(rec => (rec, 1))
        .groupBy(rec => rec._1)
        .map(rec => (rec._1, rec._2.map(r => r._2).sum))
        .map(rec => new Leaf(rec._1, rec._2))
        .toList
        .sortWith((a, b) => a.count < b.count)

      insert(res)
    }

    def codes = {
      var res = Map[Char, String]()
      def recur(node: Tree, code: String): Unit = {
        node match {
          case a: Empty =>
          case a: Node => {
            recur(a.left, code ++ "0")
            recur(a.right, code ++ "1")
          }
          case a: Leaf => res = res ++ Map((a.char, code))
        }
      }
      recur(this, "")
      res
    }

    def encode(in: String) = {
      val code = this.codes
      in.map(rec => code(rec)).mkString("")
    }

    def decode(in: String) = {
      var res = ""
      var node = this
      var iter = in
      while (iter.nonEmpty) {
        val char = iter.head
        (char, node) match {
          case ('0', n: Node) => node = node.left
          case ('1', n: Node) => node = node.right
          case (c, n: Leaf) => {
            res = res + n.char
            node = this
            if (c == '1') node = node.right else node = node.left
          }
        }
        iter = iter.tail
      }
      node match {
        case a: Leaf => res = res + a.char
      }
      res
    }
  }

  class Node extends Tree {
    var sum = 0
    var le: Tree = new Empty
    var ri: Tree = new Empty
  }

  class Leaf(a: Char, c: Int) extends Tree {
    override def char: Char = a
    override def count: Int = c
  }

  class Empty extends Tree {
  }

  def main(args: Array[String]) = {
    println("AAEEEEEFHILNRSSTTTUPP")
    val scan = new java.util.Scanner(System.in)
    val input = scan.next()
    val huff = new Node().fit(input)
    val encode = huff.encode(input)
    val decode = huff.decode(encode)
    println(huff.codes.mkString("{", ",","}"))
    println(encode)
    println(decode)
  }
}
