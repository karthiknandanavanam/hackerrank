package com.datastructures

object Contacts {

  class Trie {

    def isEmpty = {
      this match {
        case Empty => true
        case a: NonEmpty => false
      }
    }

    def increment = {
      this match {
        case Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => a.count = a.count + 1
      }
    }

    def getLink(c: Char) = {
      this match {
        case Empty => throw new UnsupportedOperationException()
        case a: NonEmpty => {
          val link = a.link.get(c)
          if (link.isDefined) {
            link.get
          }
          else {
            val ne: Trie = new NonEmpty
            a.link = a.link ++ Map(c -> ne)
            ne
          }
        }
      }
    }

    def add(name: String): Unit = {
      name match {
        case a if a.nonEmpty => val node = getLink(name.head); node.increment; node.add(a.tail)
        case _ =>
      }
    }

    def find(name: String): Int = {
      (name, this) match {
        case (_, Empty) => 0
        case (a, n: NonEmpty) if a.isEmpty => n.count
        case (a, n: NonEmpty) => getLink(a.head).find(name.tail)
      }
    }
  }

  class NonEmpty extends Trie {
    var count = 0
    var link: Map[Char, Trie] = Map()
  }

  object Empty extends Trie

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val q = scan.nextLine().toInt

    val trie: Trie = new NonEmpty
    (0 until q) foreach { _ =>
      val query = scan.nextLine()
      val queryS = query.split(" ")
      val (que, data) = (queryS(0), queryS(1))
      que match {
        case "add" => trie.add(data)
        case "find" => println(trie.find(data))
      }
    }
  }
}
