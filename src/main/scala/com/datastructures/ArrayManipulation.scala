package com.datastructures

object ArrayManipulation {
  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)

    val size = scan.nextInt()
    val changes = scan.nextInt()

    val arr = new Array[Long](size)
    for (i <- 0 until size) {
      arr(i) = 0
    }

    for (i <- 0 until changes) {
      val a = scan.nextInt()
      val b = scan.nextInt()
      val k = scan.nextLong()

      arr(a - 1) = arr(a - 1) + k
      if (b < size) arr(b) = arr(b) - k
    }

    var max = arr(0)
    for (i <- 1 until size) {
      arr(i) = arr(i) + arr(i-1)
      if (max < arr(i)) {
        max = arr(i)
      }
    }

    println(max)
  }
}
