package com.datastructures

object QHeapI {

  class Heap {
    var arr = new Array[Int](0)

    def len: Int = arr.length

    def insertKey(n: Int) = {
      arr = arr ++ Array(n)
      minHeapify(arr.length - 1)
    }

    def fullHeapify(i: Int): Unit = {
      if (i < len) {
        val parent = i
        val left = getLeft(i)
        val right = getRight(i)
        order(parent, left, right)
        fullHeapify(left)
        fullHeapify(right)
      }
    }

    def minHeapify(i: Int): Unit = {
      if (i >= 0) {
        val parent = i
        val left = getLeft(i)
        val right = getRight(i)
        order(parent, left, right)
        minHeapify(getParent(parent))
      }
    }

    def getParent(i : Int) =  {
      i match {
        case 0 => -1
        case _ => Math.ceil(i / 2.0).toInt - 1
      }
    }

    def getLeft(i: Int) = i * 2 + 1

    def getRight(i: Int) = i * 2 + 2

    def order(i: Int, left: Int, right: Int) = {
      (left, right) match {
        case (l, r) if l < len && r < len => {
          if (arr(i) > arr(l)) {
            val temp = arr(i)
            arr(i) = arr(l)
            arr(l) = temp
          }
          if (arr(i) > arr(r)) {
            val temp = arr(i)
            arr(i) = arr(r)
            arr(r) = temp
          }
        }
        case (l, r) if l >= len =>
        case (l, r) if l < len && r >= len => {
          val value = arr(i)
          val lValue = arr(l)
          if (value > lValue) {
            arr(i) = lValue
            arr(l) = value
          }
        }
      }
    }

    def display = {
      arr.mkString(" ")
    }

    def deleteKey(n: Int) = {
      var pos = -1
      for (i <- 0 until len) {
        if (arr(i) == n) {
          pos = i
        }
      }
      decreaseKey(pos, Int.MinValue)
      minHeapify(pos)
      deleteMin()
    }

    def decreaseKey(i: Int, n: Int) = {
      arr(i) = n
    }

    def deleteMin() = {
      val min = peekMin()
      arr(0) = arr(len - 1)
      arr = arr.take(len - 1)
      fullHeapify(0)
      min
    }

    def peekMin() = arr.head
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val size = scan.nextInt()

    val heap = new Heap
    for (i <- 0 until size) {
      val q = scan.nextInt()
      q match {
        case 1 => val num = scan.nextInt(); heap.insertKey(num)
        case 2 => val num = scan.nextInt(); heap.deleteKey(num)
        case 3 => println(heap.peekMin())
      }
    }
  }
}
