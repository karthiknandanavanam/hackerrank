#!/usr/bin/env bash
read a
read b

let "sum = $a + $b"
let "minus = $a - $b"
let "product = $a * $b"
let "quotient = $a / $b"

echo $sum
echo $minus
echo $product
echo $quotient